\section{Definición del problema} 
Existe una ONG llamada Farm Radio International que se encarga de recolectar y fomentar programas radiales africanos en distintos idiomas que hablen de cómo cultivar, cosechar, trabajar en granjas/campos, etc. En esta organización enfrentan un problema, el contenido que recolectan no está indexado ni clasificado por temas, y si alguien necesita identificar un programa en el que se habló de un aspecto en particular, no tienen manera de buscar en este contenido más que escuchando programas enteros.

De aquí surge la idea de poder ofrecer a un oyente la posibilidad de escuchar grabaciones de radio que contengan un tema en particular. Por ejemplo, para el caso de las plagas de langostas en campos de tomate, en aquellos programas que contengan las palabras claves (indistintamente llamadas \emph{keywords} de aquí en adelante) ``langostas'', ``tomate'', ``plaga'', se podrá escuchar momentos relevantes en los cuales se mencionan estas palabras.
 
Por lo tanto, se necesitaría desarrollar un sistema que, dado un conjunto de keywords; un corpus de programas de radio grabados directo del estudio radial sin transcripciones asociadas; y entrenadores --personas que estén dispuestas a grabar o marcar en audio existente cada keyword las veces que el sistema necesite--, devuelva las ocurrencias de estas keywords en los programas para que puedan ser indexados y ofrecidos al oyente por teléfono. 


Este sistema no permitirá búsquedas de keywords dichas por el usuario directamente, sino que se les ofrecerá una serie de temas posibles que estarán previamente asociados a ciertas keywords ya disponibles gracias al trabajo de los entrenadores. 

Una vez entrenado, si se agregaran nuevo programas, el sistema debería poder analizarlos sin requerir nuevas grabaciones de los entrenadores. En caso de agregar una keyword, el sistema podría requerir las grabaciones de los entrenadores junto a un nuevo proceso de entrenamiento y luego detectar estas nuevas keywords en los programas.

Adicionalmente, se establecerá la restricción principal del sistema que consiste en evitar utilizar grandes cuerpos de datos con transcripciones o modelos preexistentes del habla ya que se asume que el idioma con el que se trabajará no posee este tipo de recursos (como sí poseen lenguajes como el inglés o el español y muchos otros). Por lo tanto, más allá de utilizar el idioma inglés en esta tesis por poseer un corpus apropiado para el estudio, se tendrá en mente un idioma como el suajili, idioma principal del Este de África.

En base a las especificaciones anteriores, este trabajo se encargará de analizar la factibilidad de diseñar este sistema utilizando una de las técnicas de modelado más populares, hasta el momento para este tipo de herramientas, es decir, Modelos Ocultos de Markov.

En resumen, el problema que se desea resolver es el de keyword-spotting (también llamado word-spotting) que consiste en detectar palabras claves en grabaciones de habla continua donde, en la mayoría de los casos, estas palabras son grabadas para su búsqueda u obtenidas de otras grabaciones. Este problema es conocido e incluso anterior al de Reconocimiento Automático del Habla (o ASR por sus siglas en inglés).

El ASR consiste en transcribir palabras habladas a texto, y constituye un problema muy estudiado en la computación. Sus algoritmos son utilizados diariamente en todo tipo de campos como, por ejemplo, reconocimiento de comandos en teléfonos celulares y en miles de aplicaciones comerciales. El keyword-spotting puede ser considerado un sub-problema del ASR y, por lo tanto, utilizando los mismos algoritmos podemos transcribir a palabras todo el audio donde deseamos ubicar las keywords y luego buscar las ocurrencias de estas en las transcripciones.

De todas formas, este último mecanismo tiene una gran desventaja, el reconocimiento general de habla no es un problema fácil de resolver y solo funciona bien bajo una gran cantidad de supuestos. Entre estos supuestos se encuentra el de utilizar grandes cuerpos de datos de entrenamiento (es decir, habla transcripta a nivel de palabras e incluso, para mejores resultados, a nivel fonemas) que suelen escasear en la mayoría de los lenguajes, en particular lenguajes como el suajili.  

En función de lo anterior, el objetivo de este trabajo consiste en estudiar posibles alternativas para abordar este problema minimizando los recursos disponibles, tratando de emular las condiciones reales que podrían surgir en caso de intentar volver realidad el sistema antes detallado. 


\section{Trabajo Previo}
\label{sec:previo}
Tanto en reconocimiento de palabras claves como en reconocimiento general del habla, trabajos como el de \textcite{bridle1973efficient} se basaron en el uso de \emph{Dynamic Time Warping} (DTW). DTW es una técnica que utiliza programación dinámica para medir similitudes entre dos secuencias temporales que pueden tener desfasajes, en este caso, dos secuencias de audio en búsqueda de coincidencias. Esta técnica probó ser útil cuando el reconocimiento se hace sobre datos parecidos a los de entrenamiento, en cuanto se agrega variabilidad, demostró no escalar. Otro aporte importante presentada por \citeauthor{bridle1973efficient} fue la utilización de modelos de \emph{filler}, representación de palabras fuera del conjunto de palabras buscadas. 

Más recientemente, a partir del trabajo de \textcite{rabiner1989tutorial}, se ha popularizado el uso de Modelos Ocultos de Markov de distribuciones continuas utilizando palabras o sub-palabras como unidades acústicas. Desde entonces ha sido la técnica más utilizada para reconocimiento del habla. El uso de sub-palabras facilita el reuso de entrenamiento entre palabras que comparten ciertas partes permitiendo, por ejemplo, el reconocimiento de palabras fuera del conjunto de entrenamiento. 

En el caso del reconocimiento de palabras claves, el uso de estos modelos fue aplicado por primera vez por \textcite{rohlicek1989continuous} y \textcite{rose1990hidden} marcando el comienzo de numerosos estudios basados en sus ideas. Cabe destacar que el trabajo de \textcite{rose1990hidden} indica que para conseguir una buena performance se necesita entrenar un modelo de filler complejo utilizando grandes cantidades de datos transcriptos o modelos preexistentes, en particular utilizaron un HMM que conectaba completamente todo los posibles modelos de sub-unidades del lenguaje. Por otro lado, hay trabajos como el de \textcite{weintraub1993keyword} que complejizan aún más estos modelos utilizando modelos de vocabulario completo que excluyen a las keywords. 

Esta técnica funciona muy bien pero requiere una gran cantidad de datos de entrenamiento. Como se dijo en la sección anterior, el objetivo de esta tesis es evaluar un sistema pensado para utilizar la menor cantidad de datos transcriptos posibles. Trabajos como el de \textcite{moore2003comparison} muestran que un sistema de reconocimiento general disminuye hasta un 15\% de efectividad al trabajar con poca cantidad de datos (usando bases de datos de menos de 100 horas). 

Artículos como el de \textcite{samarjit2005speaker} exponen maneras de construir sistemas de detección de palabras claves utilizando la herramienta HTK. También muestra alternativas para representar fillers y modela keywords utilizando palabras como unidad acústica con Modelos Ocultos de Markov de 15 estados.   

El problema de detección de palabras claves tiene ciertas características que hacen posible realizar un sistema con corpus mucho menores a los necesarios para el reconocimiento general de habla, manteniendo una performance razonable. Existen trabajos basados en la idea de utilizar la menor cantidad de datos. Por ejemplo \textcite{thambiratnam2005acoustic} dedica un capítulo de sus tesis de doctorado en evaluar sistemas para lenguajes distintos al inglés. En particular, como se ve afectado el sistema al variar entre 164, 15 y 4 horas de entrenamiento en inglés, español e indonés. Además, compara distintos modelos para representar fonemas en las keywords, uno que utiliza \emph{trifonos}  y otros que utilizan \emph{monofonos}, explicando que solo esta última alternativa es viable para una baja cantidad de datos de entrenamiento. Su trabajo concluye que el reconocimiento de palabras claves sufre menos en performance ante la falta de datos que las tareas de reconocimiento general del habla.

También, el artículo de \textcite{garcia2006keyword} explica que con solo 15 minutos de transcripciones junto a un modelo de reconocimiento que define sus propias unidades de sonido en base a modelos segmentales se puede alcanzar una gran performance. De todas maneras, en esta tesis decidimos utilizar la manera estándar de Modelos Ocultos de Markov con distribuciones continuas que han probado funcionar muy bien con grandes cantidades de datos.

Como resultado, esta tesis se enfocará en atacar el problema utilizando Modelos Ocultos de Markov continuos para representar keywords y fillers con un esquema de filler simple y keywords entrenadas en base a apariciones en menos de 14 minutos de grabaciones transcriptas utilizando monofonos para representar los fonemas presentes en las keywords.   


\section{Estructura}
El capítulo \ref{chapter:tecnicas} introduce las técnicas utilizadas para resolver el problema de detección de palabras claves. En este capítulo se detalla la base teórica necesaria para comprender la solución planteada.

En el capítulo \ref{chapter:experimentos}, se detalla el corpus de datos utilizado para los experimentos, luego se introduce el funcionamiento del sistema junto con las métricas utilizadas en la experimentación.

En el capítulo \ref{chapter:resultados} se muestran los resultados obtenidos junto con una discusión final de estos.

La tesis concluye con el capítulo \ref{chapter:conclusiones} en el cual se realiza un resumen y se presenta el posible trabajo futuro.