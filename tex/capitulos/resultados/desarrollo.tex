
\section{Resultados sobre el conjunto de desarrollo}
En esta sección se presentan las cuatro características principales investigadas. Al finalizar, un breve resumen de estos resultados. 

Para estas pruebas, el tiempo total de las keywords en entrenamiento, es decir, la cantidad de tiempo en datos transcriptos, es de aproximadamente 14 minutos. Por otro lado, el tiempo total de grabaciones que se utilizó para el modelo de filler es aproximadamente 7 hs. El testeo se realizó sobre 1.8 horas de grabaciones en donde ocurren 313 apariciones de las keywords.

\subsection{Primera característica: Cantidad de mezclas Gaussianas}
Ya que las mezclas Gaussianas determinan las probabilidades de observación, dan flexibilidad en cuanto a reconocer mayor variedad en los vectores acústicos. Es de esperar que modelos con mayor cantidad de mezclas reconozcan con más precisión sonidos similares a los utilizados para el entrenamiento. De todas maneras, un problema que puede surgir es el de sobre-ajuste (overfitting) con respecto a estos datos. Para evitar este problema, se corroborarán los resultados en la siguiente sección. 

En los siguientes experimentos, se usarán las componentes de las mezclas como eje x en las mediciones otorgando una visión más amplia para comparar el resto de las decisiones a tomar. En general, el número de mezclas Gaussianas por estado aumentará cada cinco reestimaciones de a dos por incremento variando entre una y al menos 65 componentes.


\subsection{Segunda característica: Intervalo para fillers}
\label{sub:interv}
En la sección \ref{subsec:preparacion} hemos visto cómo se completa una transcripción utilizando fillers. Variando el intervalo tendremos fillers que se adaptan a distintas unidades del lenguaje. En nuestros experimentos variamos entre tamaños de fillers que van de 100 a 500 milisegundos. Teniendo en cuenta que 200 ms se acercan al tiempo promedio de una sílaba en inglés \cite{kent1980speech, campbell1992syllable} y 500 ms al de una palabra completa\footnote{El tiempo promedio de las palabras en este corpus es 0.48 segundos}.

Los resultados que se muestran a continuación presentan las cuatro métricas mencionadas anteriormente, en forma de tablas y gráficos, donde se pueden ver las mediciones tomadas a medida que la cantidad de mezclas aumenta.  

\clearpage
\tablegraphic{interv-fom}{Intervalo: FOM promedio}{3}

Puede observarse en esta comparación el gran incremento en el FOM promedio cuando el intervalo se acerca a 200 ms, especialmente a partir de las 25 mezclas. En cambio, los resultados obtenidos para 500 ms no mejoran al aumentar las mezclas de Gaussianas. Por otro lado, los resultados obtenidos para 100 y 350 ms, aunque lejos del óptimo, muestran una tendencia a acercarse a un valor cercano a 0.15. 

\tablegraphic{interv-fom-w}{Intervalo:  FOM ponderado}{3}

Al igual que el resultado anterior, se observa una superioridad del modelo que utiliza 200 ms como intervalo con valores superiores a 0.48. La mayor diferencia con el punto anterior consiste en el alejamiento entre los modelos de 100 y 350 ms. Ya que se está midiendo el FOM ponderado, el resultado indica que las keywords que tienen mayor cantidad de apariciones obtienen mejor resultado en el modelo de 100 ms que en el de 350 ms llegando a picos de 0.3 en el FOM. 

\clearpage
\tablegraphic{interv-hits}{Intervalo:  Porcentaje de Hits}{1}

En la medición de porcentaje de Hits se ven claramente dos momentos, el de estabilización de los modelos y luego el de refinamiento. Esto quiere decir que hasta aproximadamente diez mezclas en todos los modelos puede verse un incremento abrupto y luego un descenso leve uniforme entre los modelos que se ve acompañado por el resto de los resultados que veremos a continuación. Una vez más, los modelos que utilizan 200 ms mantienen la ventaja de manera constante una vez estabilizados. 

\clearpage
\tablegraphic{interv-fa}{Intervalo: Falsas Alarmas por hora}{1}

Las medición de falsas alarmas marca un descenso notable en todos los casos. Cabe aclarar que la mayor parte de estas falsas alarmas son inserciones. Es decir, el modelo de filler no llega a consolidarse hasta luego de estar trabajando con 20 o 30 mezclas en los mejores modelos. 

Los resultados anteriores indican una ventaja considerable en los modelos que utilizan fillers cada 200 milisegundos. En las próximas pruebas, trabajaremos con este valor fijo para seguir buscando un máximo según nuestros experimentos. 


\subsection{Tercera característica: Estados en los modelos}
A diferencia de los fonemas de keywords que son modelados usando cinco estados, para el filler, variamos entre cinco y siete estados esperando que con siete se logre mayor flexibilidad en los modelos y, por lo tanto, menor cantidad de falsas alarmas. 

A continuación, los resultados obtenidos comparando cinco contra siete estados variando la cantidad de mezclas con modelos entrenados utilizando fillers cada 200 milisegundos.

\tablegraphic{estados-fom}{Estados: FOM promedio}{3}

El primer resultado muestra una rápida adaptación del modelo de siete estados y un leve progreso en el de cinco con una marcada superioridad en especial de 30 a 40 mezclas Gaussianas. 

\clearpage
\tablegraphic{estados-fom-w}{Estados: FOM ponderado}{3}

Al igual que el gráfico anterior, se observa la ventaja de utilizar siete estados, sin mayores diferencias a pesar de estar ponderado por la cantidad de apariciones de las keywords. Cabe aclarar que aunque con una mayor cantidad de mezclas las curvas pueden llegar a unirse, el costo computacional es muy alto y por ello consideramos suficiente llegar hasta modelos con 80 mezclas. 

\clearpage
\tablegraphic{estados-hits}{Estados: Porcentaje de Hits}{1}

Con respecto al porcentaje de Hits, aunque ambos modelos alcanzan valores entre 60\% y 70\% de aciertos, el modelo de siete estados consigue estar por encima de los mejores resultados para cinco estados.
\clearpage

\tablegraphic{estados-fa}{Estados: Falsas Alarmas por hora}{1}
En falsas alarmas, el modelo de siete estados muestra obtener resultados aceptables por debajo de 100 a partir de las 30 mezclas. En cambio, el modelo de cinco estados tiene una curva mucho menos pronunciada en donde la cantidad de falsas alarmas supera por mucho a la cantidad de keywords que se desean encontrar. Es claro que la cantidad de inserciones se debe a un modelo menos flexible a diferencia del de siete estados. 

En resumen, estos resultados muestran la superioridad de los modelos que utilizan siete estados en todos los aspectos medidos.

\subsection{Cuarta característica: Unidad acústica}

Como se mencionó en la sección \ref{sec:unidad}, utilizamos dos alternativas en cuanto a la elección de unidad acústica para una keyword: palabras o fonemas. En el siguiente experimento se esperaba encontrar mejores resultados al utilizar la segunda representación dada la cantidad de datos en el corpus utilizado y las propiedades discutidas de estas alternativas. 

\tablegraphic{unit-fom}{Unidad: FOM promedio}{3}

En efecto, el FOM promedio utilizando fonemas como unidad acústica supera en gran medida a los modelos compuestos de palabras completas. Por esta razón, no mostraremos el resto de las visiones ya que la diferencia es muy grande entre estas alternativas. 

En este punto, cabe destacar que falta mucha exploración para descartar que las palabras sean una mejor unidad. El modelo que utilizamos fue un HMM left-right de 5 estados por palabra. 

\subsection{Resumen del desarrollo}
De este primer estudio obtenemos una clase de modelos que maximiza nuestras mediciones, a continuación las características:

\begin{itemize}
\item intervalo para completar con fillers de 200 milisegundos;
\item modelo de filler con siete estados (cinco emisores);
\item modelos de keywords utilizando fonemas como unidad mínima.
\end{itemize} 

\noindent En particular, el mejor resultado (según el FOM) fue devuelto por el modelo con 37 mezclas de Gaussianas que puede observarse en la tabla \ref{tab:best}. Esta tabla muestra los números alcanzados por el modelo. 

\begin{table}[H]
\centering
\begin{tabular}{cccccc}
 Mezclas & FOM promedio & FOM ponderado & \% Hits & FA/h & Apariciones \\
 37 & 0.474 & 0.47 & 68.7 & 72.7 & 313\\ 
\end{tabular}
\caption{Mejor resultado en desarrollo}
\label{tab:best}
\end{table}


