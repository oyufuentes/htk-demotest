\subsection{Obtención y métricas de resultados}
\label{sec:sistema-resultados}
Una vez que finalizó una prueba, el resultado consiste en un archivo que contiene, para cada grabación, detecciones de todas las keywords con tiempo de inicio y fin junto con un puntaje (una probabilidad) otorgado por el algoritmo de \emph{Viterbi}. Estos resultados serán comparados con la referencia que contiene las transcripciones y de esta manera se podrá calcular qué tan efectivo fue el reconocimiento. 

La manera que utilizaremos para calcular resultados será contabilizar la cantidad de aciertos (Hits) y falsas alarmas (FA) que se encuentran en el resultado. Diremos que una detección $res$ es un Hit si existe, en la referencia, una aparición de keyword $ref$ tal que la keywords es idéntica y el tiempo medio se encuentra entre el tiempo inicial y final de la detección. Es decir, $t_i(res) < t_m(ref) < t_f(res)$ donde $t_i$, $t_m$ y $t_f$ representan el tiempo inicial, medio y final de una aparición. De otra manera, esa detección constituirá una falsa alarma.

Luego, nos encargaremos de calcular la \emph{curva ROC} (Receiver Operating Characteristic), una medición estándar muy utilizada que grafica la tasa de Hits contra el número de falsas alarmas por keyword por hora (fa/kw-hr). Para reducir esta representación a un único número, se utiliza el \emph{FOM} (Figure of Merit) que equivale a la probabilidad promedio de detecciones correctas de 1 a 10 falsas alarmas por hora \cite{manos1996study}. A continuación la fórmula que utilizamos:


$$FOM = \frac{1}{10t}(\sum_{i = 1}^{n}{p_i} + \alpha p_{n+1})$$

\noindent donde
\begin{itemize}
\item $p_i$ representa el porcentaje de hits hasta la i-ésima falsa alarma\footnote{En caso de no existir la i-ésima falsa alarma, $p_i$ será el porcentaje de hits sobre el total de palabras en la referencia.};
\item $t$ el tiempo en horas de grabación;
\item $n = \lceil 10t - 0.5\rceil$ es decir, aproximadamente 10 por la cantidad de horas de grabaciones;
\item $\alpha = 10t - n$ es decir, lo que le falta a $10t - 0.5$ para llegar al entero próximo más cercano.
\end{itemize}

\noindent De esta manera, un FOM cercano a 1 indicará la detección perfecta y en cambio un FOM cercano a cero será un mal indicio de la performance de nuestro reconocedor. 

Veamos un ejemplo: dada la siguiente referencia y el resultado obtenido (tomaremos en cuenta sólo una grabación como muestra aunque se puede generalizar directamente para varias grabaciones):

\begin{multicols}{2}

\begin{verbatim}
(Referencia)
0.87  0.96  SPRINGFIELD
1.03  1.11  SUPERINTENDENT
3.12  3.18  OFFICIALS
3.31  3.34  ACCORDING
4.97  5.02  SPRINGFIELD
5.77  5.84  MELNICOVE
5.89  6.33  CELLPHONE

\end{verbatim}

\columnbreak

\begin{verbatim}
(Resultado)
1.04 1.11 SUPERINTENDENT -5254.09
3.12 3.18 OFFICIALS -4494.75
3.31 3.36 ACCORDING -3523.52
5.60 5.85 MELNICOVE -16622.43
5.89 6.23 SPRINGFIELD -3523.52
6.25 6.57 MELNICOVE -1224.33
\end{verbatim}

\end{multicols}



\noindent podemos formar la siguiente lista:
\medskip

\begin{small}
\begin{tabular}{ l l l l }
1. & SUPERINTENDENT & HIT & -5254.09\\
2. & OFFICIALS & HIT & -4494.75\\
3. & ACCORDING & HIT & -3523.52\\
4. & MELNICOVE & HIT & -16622.43\\
5. & SPRINGFIELD & FA & -3523.52\\
6. & MELNICOVE & FA & -1224.33\\
\end{tabular}
\end{small}

\bigskip
\noindent Esta lista contiene la detección junto con su resultado y su puntaje asociado\footnote{Este puntaje es el logaritmo de una probabilidad asociada al algorítmo. Para más información ver el libro de \textcite[pág. 206]{young2006htk}.}. Como se puede ver, es suficiente para calcular cantidad de aciertos y falsas alarmas por keyword.

Para calcular la ROC y el FOM asociados, primero seleccionaremos una keyword en particular y formaremos una lista equivalente a la anterior filtrando solo las apariciones de la keyword seleccionada, juntando los datos de todas las grabaciones, y ordenaremos la lista de forma decreciente según la probabilidad asociada.

Por ejemplo, para el keyword \verb|ACCORDING| podemos obtener una lista con la siguiente información: 

\medskip
\begin{small}
\begin{tabular}{ l l l l }
1. & ACCORDING & HIT & -3523.52\\
2. & ACCORDING & FA  & -3677.75\\
3. & ACCORDING & HIT & -4504.52\\
4. & ACCORDING & HIT & -10029.43\\
5. & ACCORDING & FA & -16622.52\\
6. & ACCORDING & HIT & -18622.52\\
7. & ACCORDING & HIT & -20622.52\\
8. & ACCORDING & FA & -22622.52\\
\end{tabular}
\end{small}

\bigskip
En segundo lugar, una vez ordenada, iremos computando el porcentaje de aciertos hasta un punto de corte seleccionado. Para ello, se divide el número de Hits hasta el punto de corte por el número total de apariciones de la keyword en la referencia. En principio, este punto de corte puede variar en cualquier valor desde justo antes del primer resultado hasta luego del último de la lista. Así, pueden obtenerse $|l|+1$ valores, donde $|l|$ representa el tamaño de la lista.

Por ejemplo, supongamos que la palabra \verb|ACCORDING| aparece 10 veces en la referencia. Si en la lista anterior seleccionamos el punto de corte luego del cuarto valor, el porcentaje de aciertos es $30\%$


\medskip
\begin{small}
\begin{tabular}{ l l l l }
1. & ACCORDING & HIT & -3523.52\\
2. & ACCORDING & FA  & -3677.75\\
3. & ACCORDING & HIT & -4504.52\\
4. & ACCORDING & HIT & -10029.43\\
\hline
5. & ACCORDING & FA & -16622.52\\
6. & ACCORDING & HIT & -18622.52\\
7. & ACCORDING & HIT & -20622.52\\
8. & ACCORDING & FA & -22622.52\\
\end{tabular}
\end{small}
\bigskip

Si tomamos todos los posibles valores moviendo el punto de corte y graficamos el porcentaje de Hits en función de la cantidad de falsas alarmas hasta ese punto (dividido la cantidad de horas de testeo), obtenemos la curva ROC. La figura \ref{fig:roc-according} muestra la curva ROC construida en base a la tabla anterior suponiendo 90 minutos de testeo.  

\input{capitulos/sistema/umbral}

\noindent Además, podemos calcular el FOM haciendo:\medskip

\noindent $ t = 1.5 $\\
$ n = \lceil10t - 0.5\rceil = 15$\\
$ \alpha = 10t - n = 0$\\
$ p_1 = 1/10$\\ $p_2 = 3/10$\\$ p_i = 5/10 \text{\ \ para }3 \leq i \leq 15 $\\
\smallskip

\noindent $FOM = \frac{1}{10t}(\sum_{i = 1}^{n}{p_i} + \alpha p_{n+1}) = 0.46$\\



Resumiendo, usaremos dos métricas: la curva ROC, que nos permite visualizar porcentaje de Hits a medida que aumentamos la cantidad de falsas alarmas por hora, y el FOM, que resume las primeras 10 falsas alarmas por hora de la curva ROC promediando dichos valores.

Estas métricas permiten al usuario hacer un balance entre el porcentaje de hits y la cantidad de falsas alarmas que la tarea requiera. En la curva ROC, se ve claramente el efecto en la performance del sistema al tomar como punto de corte los distintos valores del eje x.  


