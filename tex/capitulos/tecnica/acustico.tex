\section{Modelo Estadístico}
\label{sec:acustico}
Siguiendo la idea de la metáfora del canal ruidoso, se quiere descubrir el conjunto de palabras más probable $\hat{W} = \hat{w_1} \dots \hat{w_n} $ con $w_i \in \mathscr{L}$\footnote{$\mathscr{L}$ representa el conjunto de palabras del lenguaje} que puede haber generado un conjunto de observaciones acústicas $O = o_1 \dots o_t$ . Por lo tanto, lo que se quiere obtener es $\hat{W}$ tal que maximice la probabilidad $P(W|O)$. Formalmente,  

$$\hat{W} = \underset{W \in \mathscr{L}^*}{\text{argmax}}\ P(W|O)$$

\noindent utilizando Bayes, podemos convertir esta expresión en 

$$\hat{W} = \underset{W \in \mathscr{L}^*}{\text{argmax}}\ \frac{P(O|W)P(W)}{P(O)}$$

\noindent además,  $P(O)$ no depende de W, por lo tanto maximizar con o sin el dividendo es equivalente, es decir

$$\hat{W} = \underset{W \in \mathscr{L}^*}{\text{argmax}}\ \frac{P(O|W)P(W)}{P(O)} = \underset{W \in \mathscr{L}^*}{\text{argmax}}\ P(O|W)P(W)$$

Quedan así dos componentes, $P(W)$ que representa la probabilidad de una frase en nuestro lenguaje y $P(O|W)$ que indica la probabilidad de que una grabación provenga de una oración dada. Veremos cómo podemos calcular cada una y cómo, luego, calcular el argumento que maximiza la cuenta anterior. 

Para el cálculo de $P(W)$ suelen utilizarse diversas técnicas dependiendo de distintos niveles de complejidad. En general, reconocedores de habla continua suelen utilizar modelos de \emph{Trigramas} con la gran desventaja de requerir muchos datos para el entrenamiento. Ya que no contamos con ese privilegio por apuntar a lenguajes como el suajili, el modelo que utilizaremos es una simple gramática con probabilidades uniformes entre sus componentes. 



% \subsection{Hidden Markov Models}
Por otra parte, para conocer $P(O|W)$ se construyen modelos acústicos que permiten computar la probabilidad de una observación dada una unidad lingüística como por ejemplo, una palabra, un fonema, una sílaba, etc.

\subsection{Modelo Acústico}

El modelo acústico que utilizaremos en este trabajo tiene como nombre \emph{Modelo Oculto de Markov} (HMM por sus siglas en inglés), y se trata de un autómata finito compuesto por

\begin{itemize}
\item $Q = q_1 \dots q_n$ un conjunto de estados;
\item $A = a_{01} a_{02} \dots a_{1n} \dots a_{nn}$ una matriz de probabilidades de transición entre estados. Cada $a_{jk}$ representa la probabilidad de transición desde el estado $j$ y hacia el estado $k$; 
\item $O = o_1 \dots o_m$ un conjunto de observaciones;
\item $B = b_i(o_t)$ un conjunto de probabilidades de emisión, representa la probabilidad de que el estado $i$ emita la observación $t$, es decir $P(o_t|q_i)$;
\item $q_0, q_{\text{f}}$ estados especiales de inicio y fin sin observaciones asociadas.
\end{itemize}

\begin{figure}
  \centering
  \resizebox{0.8\textwidth}{!}{
    \input{capitulos/tecnica/hmm}
  }
  \caption{Estructura de un Modelo Oculto de Markov}
  \label{fig:hmm}
\end{figure}

En la figura \ref{fig:hmm} puede verse una representación general en donde se pueden observar tres estados emisores junto con el resto de las componentes de estos modelos.

Los HMMs se basan en 2 presunciones fuertes:

\begin{enumerate}
\item Suposición de Markov: $P(q_i|q_1 \dots q_{i-1}) = P(q_i|q_{i-1})$
\item Independencia de emisiones: $P(o_i|q_1 \dots q_i \dots q_n, o_1 \dots o_i \dots o_t) =  P(o_i|q_i)$ 
\end{enumerate}

\noindent Estas presunciones encajan bien con nuestro problema del habla, dadas las características que posee. Veamos un ejemplo para ver cómo esta representación permite solucionar nuestro problema.

Supongamos que contamos con un HMM especializado en reconocer la palabra ``apple''. Todavía no hemos dicho cómo se construye, pero pensemos que se trata de un HMM entrenado; es decir, $A$ (probabilidades de transición) y $B$ (probabilidades de emisión) están definidos.

Utilizaremos la figura \ref{fig:apple} para representar a este HMM que se basa en la composición fonética de \verb|APPLE|: \verb|ae p ah l|. Por ahora, cada estado representará a un fonema, luego veremos otra alternativa.

\begin{figure}[H]
  \centering
  \input{capitulos/tecnica/apple1}
  \caption{HMM para reconocer ``apple''}
  \label{fig:apple}
\end{figure}


% Para reconocer palabras usando este autómata, se necesitan una serie de pasos que detallaremos a continuación que incluye el entrenamiento y el uso de estos HMMs. 

Para comenzar el reconocimiento de la palabra, la onda sonora será convertida a una gran secuencia de vectores MFCCs, como se explicó en la sección \ref{sec:mfccs}, que será comparada contra este HMM para obtener la probabilidad de que estos sonidos hayan sido emitidos por este autómata.

Para ello, en la etapa de entrenamiento, se desarrollarán distribuciones probabilísticas para cada estado, $b_i(o_t) = P(o_t|q_i)$ donde $o_t$ es un vector MFCC y $q_i$ un estado, las cuales deberán estimar la probabilidad de que un vector de 39 componentes, proveniente de 10 milisegundos de habla humana, haya sido emitido por ese estado. 

Por este motivo, se utiliza un tipo de distribución probabilística que ha probado ser efectiva para este tipo de tareas y, por lo tanto, una de las técnicas más utilizadas: \emph{Modelos de Mezclas Gaussianas} que consta de sumar varias distribuciones normales para ajustarse a la variabilidad de los vectores acústicos que no siguen una distribución normal.

Mientras más Gaussianas se utilicen para componer una mezcla, más flexible será al momento de reconocer sonidos. Por otro lado, aumentar esta cantidad implica un mayor costo computacional y una mayor cantidad de entrenamiento.  Veremos en el capítulo \ref{chapter:experimentos} cómo variando esta cantidad de mezclas lograremos modelar keywords y fillers lo suficientemente flexibles para la detección de palabras claves.    

En resumen, cada estado contará con una distribución asociada que servirá para detectar dichos fonemas. 

\subsection{Topología de los modelos y fillers}

Al revisar un espectro de voz se puede ver que los fonemas no son uniformes a lo largo del tiempo. Si se los examina, se pueden ver tres áreas: la transición del fonema anterior al actual, la parte central y la transición al siguiente fonema. Por esta razón suele utilizarse tres estados por fonema (inicio, medio y final), consiguiendo así mejor precisión para el reconocimiento. La figura \ref{fig:apple2} muestra la modificación necesaria al esquema anterior para representar este cambio. 


\begin{figure}[H]
  \centering
  \input{capitulos/tecnica/apple2}
  \caption{HMM para reconocer ``apple'' con fonemas divididos}
  \label{fig:apple2}
\end{figure}


Puede notarse, en estos ejemplos, que cada estado contiene una transición hacia el siguiente estado y, además, un ciclo. 

Constrastando con el ejemplo de la figura \ref{fig:hmm}, no todas las transiciones han sido dibujadas, esto se debe a la topología generalmente elegida para las palabras claves, topología \emph{left-right} (llamada \emph{Bakis Network}). A diferencia de la topología \emph{ergódica} que estudiaremos para representar fillers, esta topología no permite retroceder ni saltearse estados, modelando así la naturaleza secuencial del habla. 

Los ciclos permiten que un sub-fonema se repita indefinidamente, capturando así la variabilidad en el tiempo de la entrada acústica. Según \cite{jurafsky2009speech}, el sonido de fonemas como [aa] varia de 7 a 387 milisegundos, lo cual equivale a un rango entre 1 y 38 vectores MFCC.


Hasta aquí hemos mostrado cómo un HMM puede modelar palabras claves, veamos ahora otro tipo de modelo, los fillers. 

El uso de fillers permitirá capturar cualquier sonido presente en las grabaciones, es decir, deberá ser lo más general posible. Para lograrlo, suelen utilizarse diversas técnicas como construir un solo HMM entrenado con una gran cantidad de ejemplos, también se usa la alternativa de tener múltiples fillers representando a cada fonema del lenguaje o múltiples fillers para los distintos alófonos del lenguaje, etc\footnote{ver el trabajo de \textcite{samarjit2005speaker} por ejemplo.}.

En este trabajo se utiliza un HMM ergódico, es decir, un modelo donde todos los estados que emiten estarán conectados entre sí a diferencia de la topología anterior. Si quisiera hacerse un esquema, sería similar al de la figura \ref{fig:hmm}. En ese caso, estaríamos en presencia de un modelo de filler de cinco estados (tres emisores).

% Las alternativas estudiadas en este trabajo utilizan modelos de fillers de cinco y siete estados, en donde, se analizan distintas cantidades de mezclas Gaussianas por estado, en general superior a la cantidad en los modelos de fonemas, suponiendo que mayor cantidad de mezclas producirán más flexibilidad en la detección cumpliendo con el objetivo de estos modelos. 

\subsection{Entrenamiento y Decodificación}

Una vez que está clara la estructura que se utilizará para cada modelo, se procede a la etapa de entrenamiento, es decir, la estimación de los parámetros del HMM. La manera de entrenar el modelo será mediante el algoritmo de Baum-Welch llamado \emph{forward-backward}, un algoritmo iterativo que intenta encontrar los parámetros que maximicen la probabilidad de haber generado los datos de entrenamiento. Este algoritmo estima las probabilidades asociadas a cada estado y las probabilidades de transición entre estados. 

% \begin{figure}
% \begin{multicols}{2}
% \resizebox{0.8\columnwidth}{!}{
% \input{capitulos/tecnica/empty_apple}
% }
% \hspace{-1cm}$\xrightarrow{forward-backward}$
% \columnbreak
% \resizebox{0.8\columnwidth}{!}{
% \input{capitulos/tecnica/apple1}
% }
% \end{multicols}
% \caption{Algoritmo Baum-Welch}
% \end{figure}

Como entrada, recibe grabaciones etiquetadas a nivel fonemas que le permitirán asociar sonidos con los estados correspondientes y sus transiciones. En el caso de las keywords, suponiendo que no dispondremos de transcripciones a nivel fonema, pero sí a nivel palabras, se divide cada palabra uniformemente según diccionarios fonéticos. 

Este último paso es posible en lenguajes en los que se dispone de dicho diccionario. Para los casos en que no se disponga, habrá que hacer un estudio del lenguaje para ver si es factible utilizar la traducción ortográfica y luego mapear a los fonemas correspondientes. En el caso del español y el suajili es razonable efectuar dicha operación por las características de estos lenguajes, en los cuales existe un mapeo casi inmediato de letras a sonidos. 

Finalmente, cuando nuestros modelos están entrenados se los conecta utilizando el modelo de lenguaje (como habíamos mencionado anteriormente, mediante una gramática con probabilidades uniformes) quedando listo para la tarea de reconocimiento.

\begin{figure}[H]
    \resizebox{0.6\textwidth}{!}{
        \input{capitulos/tecnica/grammar}
    }
    \caption{Gramática de estados finitos}
    \label{fig:grammar}
\end{figure}

La gramática de estados finitos mostrada en la figura \ref{fig:grammar} refleja los posibles estados en los que puede estar el proceso de reconocimiento sobre una grabación. Este tipo de gramáticas es una de las alternativas existentes para modelar problemas de reconocimiento de keywords. En este caso en particular, se permite una cantidad indeterminada de repeticiones de keywords en donde puede o no haber apariciones de fillers intercaladas. 

La figura \ref{fig:viterbi} muestra una ampliación de la gramática utilizando dos keywords (``one'' y ``five'') junto a los HMMs vistos sobre el cual el algoritmo actúa\footnote{En este esquema no se dibujaron los estados no emisores para simplificar la explicación}.

\begin{figure}[H]
\resizebox{\textwidth}{!}{
\input{capitulos/tecnica/viterbi}
}
\caption{HMMs unificadas}
\label{fig:viterbi}
\end{figure}

El algoritmo de \emph{Viterbi} (decodificación), recorre el HMM buscando el mejor camino respecto de un conjunto de datos otorgados que seguirán siendo MFCCs pero esta vez sin transcripciones asociadas. De esta manera, devuelve las palabras que se forman utilizando el camino más probable junto con un puntaje que utilizaremos más adelante.  
