$LOAD_PATH << './lib/'
$LOAD_PATH << './lib/hparse_alternative/'
Dir["./lib/**/*.rb"].each {|f| require f}
require 'pry'
require 'terminal-table'

Log.start "./thor-log/", quiet: true

class KS < Thor
  include Thor::Actions

  method_options :print_commands => :boolean
  method_option :descending, desc: "sorting direction, ascending if not specified"
  method_option :sort_by, desc: "field to sort by"
  method_option :group_by, desc: "group by"
  method_options :latex => :boolean
  def initialize(*args); $print_commands = args.any? {|x| x.include? "--print-commands"} ; super; end

  no_tasks do
    def keywords(sample)
      Keywords.sample_data(sample)
    end

    def all_words
      Keywords.all_words({
        wrds_source: "./data/corpus/",
        dictionary: "./sources/dictionary/dict"
      })
    end

    def fetch_kind(type = :train)
      kind = options[:kind].downcase.to_sym
      kinds = type == :train ? [:recognizer, :merged, :keywords, :filler] : [:recognizer, :merged, :split]
      raise "invalid kind for #{type}, expecting: #{kinds} got #{kind} (#{kind.class})" unless kinds.include? kind
      kind
    end

    def best_results(all)
      models = all.map {|x| x[:folder].gsub(/hmm_\d+/,"")}.uniq
      to_keep = []
      models.each do |m|
        to_keep << all.select {|x| x[:folder] =~ /#{m}hmm_\d+/}.max {|x| x[:fom]}
      end
      to_keep
    end

  end
  ####################### SETUP ... Corpus ----> Wavs ###################################
  desc "setup CORPUS_FOLDER CORPUS_DESTINATION_FOLDER WAV_LIST_FILE",
  "extracts the corpus into CORPUS_DESTINATION_FOLDER, using as source CORPUS_FOLDER, " +
  "it also generates a list of the imported files in the " +
  "parameter WAV_LIST_FILE"
  method_option :clean, desc: "Cleans the CORPUS_DESTINATION_FOLDER folder before copying files", default: false
  def setup(corpus_path, corpus_destination, wav_list)
    Log.on corpus_destination

    clean corpus_destination if options[:clean]
    Corpus.extract corpus_path, corpus_destination, extension_to_find: 'wrd' do |file, dst|

      if File.extname(file) == ".wav"
        safe_system_call("sox #{file} #{dst}/#{File.basename(file)}")
      elsif File.extname(file) == ".sph"
        safe_system_call("sox #{file} #{dst}/#{File.basename(file.gsub('.sph','.wav'))}")
      else
        FileUtils.cp(file, dst)
      end

    end

    Scripts.generate_list wav_list, corpus_destination
  end


  ####################### GENERATE_MFCCS ... Wavs ----> MFCCs ###################################

  desc "generate_mfccs MFCCS_DESTINATION WAV_LIST_FILE", "extracts mel frecuency ceptral coefficients from the wav list into the destination folder"
  method_option :clean, desc: "Cleans the MFCCS_DESTINATION folder before copying files", default: false
  def generate_mfccs(dst, wav_list)
    Log.on dst

    clean dst if options[:clean]
    FileUtils.mkdir_p dst
    Scripts.generate_for_hcopy wav_list, dst, "scripts/HCopy.script"
    say HTK.hcopy("scripts/HCopy.script"), :green
    Scripts.generate_list "#{dst}/mfccs.list", dst, 'mfc'
  end

  desc "create_lab_files WRDS_FOLDER DESTINATION", "extracts lab files from wrds"
  method_option :corpus, desc: "name of the corpus to extract labs from", default: "boston"
  def create_lab_files(wrds_folder, dst)
    transcriptions_handler = TranscriptionsHandler.new

    transcriptions_handler.fetch_transcriptions_from_wrds({
      wrds_folder: wrds_folder,
      save_labs_in: dst,
      corpus: options[:corpus]
    })
  end

  desc "generate_lists MFCCS_LIST TRAINING_LIST TESTING_LIST", "generate cross validation files"
  method_option :partition, desc: "cross validation fold to be used (1-5)", required: true, type: :numeric
  def generate_lists(mfccs_list, training, testing)
    Scripts.generate_lists({
      training_list: training,
      testing_list: testing,
      mfccs_list: mfccs_list,
      partition: options[:partition]
    })
  end


  desc "prepare_results_lists MFCCS_LIST DESTINATION", "generate results lists"
  def generate_results_lists(mfcc_list, dst)
    Scripts.generate_results_lists(mfcc_list, dst)
  end

  ####################### TRAIN ... MFCCs ----> Models (HMMs) ###################################

  desc "train MFCCS_FOLDER", "runs the complete training of the models"
  method_option :kind, aliases: "-k", desc: "Kind of training, options: recognizer, keywords, filler, merged", required: true
  method_option :states, aliases: "-s", desc: "Number of states used in the HMMs", default: 5, type: :numeric
  method_option :filler_states, desc: "Number of filler states used in the HMMs (for merged and splitted models only)", default: 5, type: :numeric
  method_option :filler_kind, desc: "ergodic or left-right models", default: "erdgodic"
  method_option :reestimations, aliases: "-r", desc: "Number of reestimations", default: 10, type: :numeric
  method_option :increment_mixtures_every, aliases: "-m", desc: "Intercalates mixtures duplication every x reestimations", default: 4, type: :numeric
  method_option :increment_mixtures_by, desc: "defines how to update mixtures for keywords, examples: '+2' '*3'", default: "+2"
  method_option :increment_filler_mixtures_by, desc: "defines how to update filler mixtures", default: "+2"
  method_option :name, aliases: "-n", desc: "sets the models/data name", required: true
  method_option :training_list, desc: "list that contains the files to be used in training", default: "./data/training.list"
  method_option :testing_list, desc: "list that contains the files to be used in testing", default: "./data/testing.list"
  method_option :word_as_unit, desc: "Use words instead of phonemes as acustic units", default: false
  method_option :fill_using, desc: "Fillers can be added instead of words or using an interval, options: \"words\" or \"intervals\"", default: "intervals"
  method_option :filler_every, desc: "Fillers interval", default: 0.5, type: :numeric
  method_option :keywords_sample, desc: "Choose between keywords samples", default: 1, type: :numeric
  def train(mfccs_folder)
    kind = fetch_kind
    raise "Invalid mfccs_folder" unless File.exists?(mfccs_folder)

    name = options[:name].downcase
    train_folder = "./trained/#{name}"

    Log.on train_folder
    Log.write "Training called with argument: #{mfccs_folder} and options: #{options}"

    training_options = {
      on: "#{train_folder}/models/",
      copy_data_on: "#{train_folder}/data/",
      dictionary: "./sources/dictionary/dict",
      filler_every: options[:filler_every],
      fill_using: options[:fill_using].to_sym,
      filler_kind: options[:filler_kind] == "ergodic" ? :ergodic : :left_right,
      filler_states: options[:filler_states],
      herest_scripts_path: "#{train_folder}/scripts/HERest/",
      increment_filler_mixtures_by: options[:increment_filler_mixtures_by],
      increment_mixtures_by: options[:increment_mixtures_by],
      increment_mixtures_every: options[:increment_mixtures_every],
      mfccs_folder: mfccs_folder,
      phones_dict: "#{train_folder}/data/phones.dict",
      phones_list: "#{train_folder}/data/phones.list",
      reestimations: options[:reestimations],
      split_training_every: 100,
      states: options[:states],
      testing_list: options[:testing_list],
      training_list: options[:training_list],
      training_phones_mlf: "#{train_folder}/mlfs/train-phones.mlf",
      training_words_mlf: "#{train_folder}/mlfs/train-words.mlf",
      word_as_unit: options[:word_as_unit],
      words_sorted: "#{train_folder}/data/words-sorted.list",
    }

    training_options.merge!(keywords: keywords(options[:keywords_sample])) if kind == :keywords || kind == :merged
    training_options.merge!(all_words: all_words) if kind == :recognizer

    Training.train(kind, training_options)

  end

  ####################### TEST ... Models ----> Logs  ###################################

  desc "test MODEL DATA_FOLDER", "it tests the model, using the one passed as argument on MODEL retreiving data from DATA_FOLDER (phones_list, phones_dict, eg)"
  method_option :force, desc: "Runs the test without checking, rewrites old logs for the same test name", default: false
  method_option :transition_penality, aliases: "-p", desc: "Transition penality on the grammar (could change Viterbi algorithm result)", default: 0, type: :numeric
  method_option :likehoods_scaling, aliases: "-l", desc: "Transition penality scaling", default: 5, type: :numeric
  method_option :prunning, desc: "Prunning value", type: :numeric
  method_option :test_name, aliases: "-n", desc: "sets the test name", required: true
  method_option :kind, aliases: "-k", desc: "Kind of testing, options: recognizer or merged", required: true
  method_option :filler_model, desc: "Filler model used on split testing"
  method_option :filler_data_folder, desc: "Filler data folder used on split testing"
  method_option :filler_probability, desc: "Filler probability", type: :numeric
  method_option :samples, desc: "Number of files to be tested as max, all by default"
  method_option :testing_list, desc: "File containing the list of mfccs to be tested. If none, the data folder will be scaned looking for testing.list file"
  def test(model, data_folder)
    kind = fetch_kind :test

    test_name = options[:test_name]
    test_folder = "./tests/#{test_name}"
    testing_list = options[:testing_list] || "#{data_folder}/testing.list"
    raise "Test already exists" if File.exists?(test_folder) && !options[:force]

    Log.on test_folder
    Log.write "Testing called with argument: model: #{model}, data_folder: #{data_folder} and options: #{options}"

    clean test_folder

    testing_options = {
      model: "#{model}/hmmdefs",
      macros: "#{model}/macros",

      phones_list: "#{data_folder}/phones.list",
      phones_dict: "#{data_folder}/phones.dict",
      words_sorted: "#{data_folder}/words-sorted.list",

      grammar: "#{test_folder}/grammar",
      grammar_wdnet: "#{test_folder}/grammar.wdnet",

      testing_list: testing_list,
      results: "#{test_folder}/detected.mlf",

      reference_mlf: "#{test_folder}/reference.mlf",
      transition_penality: options[:transition_penality],
      likehoods_scaling: options[:likehoods_scaling],
      prunning: options[:prunning],
      samples: options[:samples]
    }

    case kind
    when :recognizer
      testing_class = RecognizerTesting
      testing_options.merge! all_words: all_words
    when :merged
      testing_class = MergedTesting
      testing_options.merge! filler_probability: options[:filler_probability]
    when :split
      raise "filler model missing" unless options[:filler_model]
      raise "filler data missing" unless options[:filler_data_folder]

      filler_model = options[:filler_model]
      filler_data_folder = options[:filler_data_folder]

      testing_options.merge!({
        keywords_model: "#{model}/hmmdefs",
        filler_model: "#{options[:filler_model]}/hmmdefs",
        model: "#{test_folder}/model/merged/hmmdefs",

        keywords_phones_list: "#{data_folder}/phones.list",
        keywords_phones_dict: "#{data_folder}/phones.dict",
        keywords_words_sorted: "#{data_folder}/words-sorted.list",

        filler_phones_list: "#{filler_data_folder}/phones.list",
        filler_phones_dict: "#{filler_data_folder}/phones.dict",
        filler_words_sorted: "#{filler_data_folder}/words-sorted.list",

        phones_list: "#{test_folder}/phones.list",
        phones_dict: "#{test_folder}/phones.dict",
        words_sorted: "#{test_folder}/words-sorted.list",

        filler_probability: options[:filler_probability],
      })

      testing_class = SplitTesting
    end

    testing = testing_class.new(testing_options)

    logging_execution_time("Starting with #{test_name} (#{kind}) at #{Time.now}") do
      testing.start
    end
  end
	
  desc "automate MERGED_TRAINED_NAME FROM TO BY [FORCE]", "automate tests using hmm_FROM...hmm_TO (by BY)"
  method_option :testing_list, desc: "File containing the list of mfccs to be tested. If none, the data folder will be scaned looking for testing.list file"
  method_option :name_prefix, desc: "add a prefix to the tests name", default: ""
  def automate(name, data_name, from, to, by, force = false)
   (from.to_i..to.to_i).step(by.to_i).each do |n|
      force_flag = force ? "--force" : ""
      test_name = "#{options[:name_prefix]}#{name}_hmm_#{n}"
      if File.exists?("./tests/#{test_name}") && !force
        puts "skping test '#{test_name}', reason: already exist"
      else
        puts "starting with test #{test_name}"
        testing_list_flag = "--testing-list #{options[:testing_list]}" if options[:testing_list] 
        safe_system_call("thor ks:test -k merged #{testing_list_flag} --test-name '#{test_name}' './trained/#{name}/models/hmm_#{n}' './trained/#{data_name}/data' #{force_flag}") 
      end
    end
  end

  desc "dot GRAMMAR_WDNET", "generates a dot graphic representation of the grammar"
  def dot(grammar)
    res = "digraph grammar {\n"
    res << "rankdir=LR;\n"
    nodes = {} 
    grammar.each do |l|
      next unless l.start_with? "I"
      id, name = l.split.map {|x| x.split("=")[1]}
      nodes[id] = name.gsub("-","_").gsub("!","")
    end
    nodes.each do |id, name|
      res << "#{id} [label=\"#{name}\"];\n"
    end
    nodes.each do |id, name|
      grammar.each do |l|
        next unless l.start_with? "J"
        (connection_id, start, endd, prob) = l.split.map {|x| x.split("=")[1]}
        if start == id
          res << "#{start} -> #{endd}" + (prob ? "[label=\"#{prob}\"]" : "") +  ";\n" 
        end

      end
    end
    res << "}\n"
    res.save_as "#{grammar.dirname}/grammar.dot"
  end

  desc "results TEST_FOLDER", "returns the test results"
  method_option :show_details, desc: "show extra details", default: false
  method_option :total_tests_time, aliases: "-t", desc: "tests total time in hours", type: :numeric
  method_option :ignore_log_probabilities, desc: "ignore the log probabilities associated to each detection", default: false
  def results(test_folder)
    #sample: thor ks:results ./tests/merged_training_sample1_word_as_unit_7_states_filler_hmm_20/ --show-details  --sort-by insertions --show raw
    reference = "#{test_folder}/reference.mlf"
  
    comparation = Results.new(results_mlf: "#{test_folder}/detected.mlf").compare_with reference, options
    
    rows = []
    comparation.each do |word, p|
      rows << {word: word}.merge(p)
    end

    summary = Results.summarize(comparation)
    table = rows.as_table(options.merge({summary: summary, title: "Results for #{test_folder}"}))
    
    puts table
  end

  desc "join_results DST {RESULTS}", "join results and references"
  def join_results(dst, *results)
    FileUtils.mkdir_p dst
    res_mix = ""
    ref_mix = ""
    results.each do |result_folder|
      ref_mix << File.read("#{result_folder}/reference.mlf")
      res_mix << File.read("#{result_folder}/detected.mlf")
    end
    (dst.cd "reference.mlf").write ref_mix
    (dst.cd "detected.mlf").write res_mix
  end

  desc "total_time FILES_LIST", "calculates wavs total_time"
  def total_time(list)
  	time = Common.total_time_in_seconds(list)/3600.0
  	puts time.round(3)
  end

  method_option :fom, desc: "show only fom values"
  method_option :only_for, desc: "show only values for that keyword"
  desc "roc curve for TEST_FOLDER", "returns the roc analysis for an specific keyword"
  def roc(test)
    reference = "#{test}/reference.mlf"
    results = "#{test}/detected.mlf"
    word_list = "#{test}/words-sorted.list"

    res = {}
    word_list.each do |word|
      next if word == "FILLER"
      next if word.downcase != options[:only_for].downcase if options[:only_for]

      res[word] = Results.roc(results, reference, word, options)
    end
    if options[:latex]
      puts LatexHelper.roc res
    else
      puts res
    end

  end

  desc "all_results FOLDER", "searchs for results on subfolders"
  method_option :only_for, desc: "leave only 1 word"
  method_option :only, desc: "leave files starting with that name"
  method_option :total_tests_time, aliases: "-t", desc: "tests total time in hours", type: :numeric
  method_option :best_results, desc: "join results ignoring reestimations", type: :boolean
  method_option :ignore_log_probabilities, desc: "ignore the log probabilities associated to each detection", default: false
  method_option :plot, desc: "plot latex graphs using a field as Y"
  
  def all_results(folder = "./tests/")
    Log.stop
    all = []
    Dir.glob("#{folder}/**").each do |test_folder|

      if File.exists? "#{test_folder}/reference.mlf"
        next unless !options[:only] || test_folder.basename.include?(options[:only])
        results = "#{test_folder}/detected.mlf"
        cleaned_results = "#{test_folder}/detected-cleaned.mlf"
        reference = "#{test_folder}/reference.mlf"
        words_list = "#{test_folder}/words-sorted.list"
        hmmdefs = "#{test_folder}/hmmdefs"

        begin
         	comparition = Results.new(results_mlf: "#{test_folder}/detected.mlf").compare_with reference, options
        	File.write "#{test_folder}/results.text", comparition
		    	all << {folder: test_folder.basename, filler_mixtures: Mixtures.on(hmmdefs, :filler), keywords_mixtures: Mixtures.on(hmmdefs, :keywords)}.merge(Results.summarize(comparition))
          #, test_files: "#{test_folder}/files.list".count_lines
      	rescue
      		all << {folder: test_folder.basename, error: true}
      	end
      end
    end

    all = best_results(all) if options[:best_results]
    if options[:plot]
      LatexHelper.plot(all, options[:plot].to_sym)
    else
      puts all.as_table(options.merge({title: "Results from #{folder}"}))
    end
  end

  desc "keywords_info FILES_LIST", "shows statistics about keywords in the list of files, it will find for lab files on the same folder"
  method_option :top, desc: "take only n top samples", type: :numeric
  method_option :min_length, desc: "restrinct keywords by size", type: :numeric
  method_option :keywords_sample, desc: "show information about corresponging keyword sample", type: :numeric
  def keywords_info(mfccs_list)
    res = Statistics.from_mfccs_list(mfccs_list)
    res = res.select {|x,y| keywords(options[:keywords_sample]).include? x.downcase} if options[:keywords_sample]
    res = res.select {|x,y| x.size >= options[:min_length]} if options[:min_length]
    res = res.sort_by {|x,y| y[:count]}.reverse
    res = res.take options[:top] if options[:top]
    res.map! {|k,v| {keyword: k}.merge v}
    puts res.as_table(options)
  end

  desc "summary TRAINING_FOLDER [TESTS_FOLDER]", "shows information about trained models, optionally, count associated tests"
  method_option :only, desc: "leave files starting with that name"
    def summary(trained_folder = "./trained/", tests_folder = "./tests")
    res = []
    Dir.glob("#{trained_folder}/**").each do |folder|
      next unless !options[:only] || folder.basename.include?(options[:only])

      models = Dir.glob "#{folder}/models/**"
      last_model = models.sort_by {|m| m.split("_").last.to_i}.last
      last_hmmdefs = "#{last_model}/hmmdefs"
      data = {name: folder.basename, training_files: "#{folder}/data/training.list".count_lines , last_model: last_model.basename}

      if File.exists? last_hmmdefs
        data.merge! keywords_mixtures: Mixtures.on(last_hmmdefs, :keywords), filler_mixtures: Mixtures.on(last_hmmdefs, :filler)
      else
        data.merge! not_ready: true
        previous_hmmdef = last_hmmdefs.gsub(/hmm_\d+/) { |text| "hmm_" + (text.split("_").last.to_i - 1).to_s}
        if File.exists? previous_hmmdef
          data.merge! keywords_mixtures: Mixtures.on(previous_hmmdef, :keywords), filler_mixtures: Mixtures.on(previous_hmmdef, :filler)
        end
      end
      if tests_folder
        tests = Dir.glob("#{tests_folder}/#{folder.basename}*")
        similar_training_folder = Dir.glob("#{trained_folder}/#{folder.basename}*").without folder
        similar_training_folder.each do |similar|
          tests = tests - Dir.glob("#{tests_folder}/#{similar.basename}*")
        end
        data[:associated_tests] =  tests.count
      end
      res << data
    end
    puts res.as_table(options.merge({title: "Training Summary"}))
  end


  desc "drop SOURCE DEST","export models and tests into dest folder"
  def drop(source, dst)
    Common.drop source.cd("trained"), source.cd("tests"), dst 
  end
end






