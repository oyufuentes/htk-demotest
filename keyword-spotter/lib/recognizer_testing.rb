require 'testing.rb'

class RecognizerTesting < Testing

  def initialize(options)
    require_and_load(options,
      :model,
      :phones_list,
      :phones_dict,
      :all_words
    )
    super
  end

  def start
    prepare_grammar
    test
    prepare_reference_file
  end

  def prepare_grammar
    grammar = Grammar.new(:recognizer, {
      grammar: @grammar,
      grammar_wdnet: @grammar_wdnet,
      all_words: @all_words
    })
    grammar.create
    grammar.generate_wordnet
  end

  def prepare_reference_file
    transcriptions_handler = TranscriptionsHandler.new
    transcriptions_handler.generate_master_label_files(:recognizer, {
      words_mlf: @reference_mlf,
      files_list: @testing_list,
      all_words: @all_words
    })
  end

end