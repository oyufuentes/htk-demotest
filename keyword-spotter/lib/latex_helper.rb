class LatexHelper
	def self.roc(results)
  	#http://www.sharelatex.com/learn/Pgfplots_package
  	xmax = results.map {|k,l| l.count}.max
  	xticks = (1..xmax).to_a.join(",")
  	yticks = (0..1).step(0.1).to_a.map {|x| x.round 2}.join(",")
  	
  	coordinates = []
  	#   xtick={#{xticks}},
  	# ytick={#{yticks}},
  	# 		legend pos=outer north east

  	pre = "
  	\\begin{tikzpicture}[font=\\tiny]
  	\\begin{axis}[
  	xlabel={umbral},
  	ylabel={\\% Hits},
	width=\\textwidth,
  	xmin=0, xmax=#{xmax},
  	ymin=0, ymax=1,
  	ymajorgrids=true,
  	grid style=dashed,
    	legend style={nodes=right},
  	legend pos=north west
  	]
  	"

  	plots = ""
  	results.each do |key, res|
  	coord = res.each_with_index.map { |x,i| "(#{i},#{x})" }.join("")

  	plots << "\\addplot
  	coordinates {
  	#{coord}
  	};
        \\addlegendentry{#{key}}
  	"
  	end

  	pos = "\\end{axis}
  	\\end{tikzpicture}"

  	return pre + plots + pos  
	end

  def self.plot(rows, field)
    rows.each do |r|
      puts "(#{r[:keywords_mixtures]},#{r[field]})"
    end
  end

  def self.table(target)
    t = target.number_of_columns
    res = "\\begin{table}\n\\begin{tabular}{#{'l'*t}}\n"
    res << target.headings.cells.join("&") << "\\\\\n"
    target.rows.each do |r| 
      res << r.cells.join("&") << "\\\\\n"
    end
    res << "\\end{tabular}\n"
    res << "\\caption{#{target.title}}\n"
    res << "\\end{table}"
    res
  end
end
