require 'training'

class KeywordsTraining < Training
  def initialize(options)
    super(options)
    require_and_load(options,
      :keywords,
      :dictionary
    )
    
    options[:words] = @keywords
  end

  def kind  
    :keywords
  end

end



