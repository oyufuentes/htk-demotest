class Dictionary

  def self.prepare_for(kind, options)
    require_and_load(options, :words_sorted, :phones_list, :phones_dict)
    @words = options[:words]
    @dictionary = options[:dictionary]
    @word_as_unit = options[:word_as_unit]

    FileUtils.mkdir_p File.dirname(@words_sorted) if @words_sorted
    FileUtils.mkdir_p File.dirname(@phones_dict) if @phones_dict
    FileUtils.mkdir_p File.dirname(@phones_list) if @phones_list

    send "prepare_for_#{kind}"
  end

  def self.prepare_for_merged
    prepare_for_filler
    prepare_for_keywords
  end

   def self.prepare_for_recognizer
    prepare_for_keywords
  end

  def self.prepare_for_filler
    @words_sorted.write "FILLER"
    @phones_dict.write "FILLER\tfill"
    @phones_list.write "fill" 
  end  

  def self.prepare_for_keywords
    @words.sort.each do |k|
      @words_sorted.write k.upcase, append: true
      @phones_dict.write "#{k.upcase}\t#{k.downcase} sp", append: true if @word_as_unit
      @phones_list.write "#{k.downcase}", append: true if @word_as_unit
    end

    unless @word_as_unit
      HTK.hdman({
        words_sorted: @words_sorted,
        phones_list: @phones_list,
        phones_dict: @phones_dict,
        dictionary: @dictionary
      })
      
      check_balance
    end

    @phones_dict.write "SENT-START [] sil", append: true
    @phones_dict.write "SENT-END [] sil", append: true
    @phones_dict.write "SILENCE sil", append: true
    @phones_list.write "sil", append: true

    [@words_sorted, @phones_dict, @phones_list].each do |f|
      f.sort_file
    end

    dict = ""
    @phones_dict.each do |l|
      dict.add(l.include?("FILLER") ? l.gsub("sp", "") : l)
    end
    dict.save_as @phones_dict

    phones_list_without_sp = File.read(@phones_list).gsub("sp\n", "")
    File.open(@phones_list, "w") {|file| file.puts phones_list_without_sp }
  end

  def self.check_balance
    hdman_log = @phones_dict + ".log"
    counts = hdman_log.readlines_from("New Phone Usage Counts", "").drop 2
    counts.select! {|x| x.split[3].to_i < 3}
    unless counts.empty? || $testing
      puts "\nWARNING: The dictionary is NOT a Phonetically Balanced dictionnary. It's required to add additional words to make sure we have 3-5 occurences of each phoneme in our Pronunciation Dictionnary." 
      puts "\nPhonemes with insufficient data:"
      puts counts
    end
  end
 
end

