class TranscriptionsHandler

  def fetch_transcriptions_from_wrds(params)
    require_and_load(params, :wrds_folder, :save_labs_in)

    raise "wrds source folder doesn't exist: #{@wrds_folder}" unless File.exist? @wrds_folder
    corpus = params[:corpus] || :boston
    send "from_wrds_to_labs_#{corpus}"
  end

  def generate_master_label_files(kind, params)
    @params = params
    require_and_load(params, :files_list, :words_mlf)
    @kind = kind

    case @kind
    when :keywords
      require_and_load(params, :keywords)
      @keywords.map! &:downcase
    when :merged
      require_and_load(params, :keywords, :fill_using)
      require_and_load(params, :interval_in_seconds) if @fill_using == :intervals
      @keywords.map! &:downcase
    when :recognizer
      require_and_load(params, :all_words)
      @keywords = @all_words.map &:downcase
    when :filler
      require_and_load(params, :fill_using)
      require_and_load(params, :interval_in_seconds) if @fill_using == :intervals
    end

    @phones_mlf = params[:phones_mlf]
    @phones_dict = params[:phones_dict]

    generate_word_level_mlf
    create_phone_level_mlf(kind, params) if @phones_mlf
  end

  def create_phone_level_mlf(kind, params)
    require_and_load(params, :phones_dict, :phones_mlf, :words_mlf)
    HTK.hled({
      phones_dict: @phones_dict,
      phones_mlf: @phones_mlf,
      kind: kind,
      words_mlf: @words_mlf,
      include_sp: params[:include_sp] || false,
    })
  end

  private

  def generate_word_level_mlf
    FileUtils.mkdir_p(File.dirname(@words_mlf))
    File.open(@words_mlf, "w") do |io|
      io << "#!MLF!#\n"
      foreach_in_list(@files_list).each do |mfcc|
        lab = mfcc.gsub(".mfc",".lab")
        raise "lab files need to be on the mfccs folder for this method to work, missing: #{lab}" unless File.exist? lab
        text = File.read(lab).gsub(/(\d*\.\d*)/) {|n| "%09d" % (n.to_f*10000000).to_i}
        transcription = process_transcription(text)
        io.write("\"#{lab}\"\n#{transcription}.\n") unless transcription.strip.empty?
      end
    end
  end

  def process_transcription(text)
    crud_trascriptions = text.split("\n")
    transcriptions = []

    unless @kind == :filler
      transcriptions = crud_trascriptions.map {|l| [l.split[0], l.split[1], l.split[2].gsub(/[^a-zA-Z\n]*/,"")]}
      transcriptions.select! {|l| @keywords.include? l[2].downcase}
    end

    unless @kind == :recognizer || @kind == :keywords
      transcriptions = fill(transcriptions, crud_trascriptions)
    end
    transcriptions.map {|l| l.join "  "}.join("\n") << "\n"
  end

  def fill(transcriptions, crud_trascriptions)
    res = []
    raise "bad parameter on #{@params} (fill_using: #{@fill_using})" if @fill_using && @fill_using != :words && @fill_using != :intervals
    if @fill_using == :words
      crud_trascriptions.each do |crud|
        data = crud.split
        transcriptions << [data[0], data[1], "FILLER"] unless transcriptions.any? {|x| x[0] == data[0]}
      end
      transcriptions
    else
      from = 0
      transcriptions.each do |transc|
        to = transc[0].to_i
        res << fill_interval(from, to)
        from = transc[1].to_i
      end
      end_time = crud_trascriptions.last.split[1].to_i
      res << fill_interval(from, end_time) if from != end_time
      res << transcriptions
      res.flatten(1).sort_by {|x| x[0].to_i}
    end
  end

  def fill_interval(from, to)
    interval = @interval_in_seconds * 10000000
    counter = from
    res = []
    while counter + interval < to do
      res << ["%09d" % counter, "%09d" % (counter + interval), "FILLER"]
      counter = counter + interval
    end
    res << ["%09d" % counter, "%09d" % to, "FILLER"]
    res
  end

  #  For boston corpus wrds format (measured in seconds)
  #  Example:
  # -------------------------------------------------
  #  signal /u9/smg/data/radio/f1a/radio/st01/f1ast01p3
  #  type 1
  #  color 76
  #  font -*-times-medium-r-*-*-17-*-*-*-*-*-*-*
  #  separator ;
  #  nfields 1
  #  #
  #
  #      0.090000   76 a
  #      0.570000   76 bail-out
  #      0.950000   76 plan
  #      ...
  # ------------------------------------------------

  def from_wrds_to_labs_boston
    Log.write "Extracting labs for Boston Corpus"
    foreach_file_in @wrds_folder, 'wrd' do |wrd|
      sharp_spotted = false

      name = File.basename(wrd).sub(".wrd","")
      folder = File.dirname(wrd)

      lab_name = "#{@save_labs_in}/#{name}.lab"
      File.open(lab_name,'w') do |out|
        previous_time = "0.00" #TODO: check if starting with 0.05 fix silence problems.
        File.open(wrd).each do |line|
          if sharp_spotted
            begin
              line = clean_encode_problems(line)
              next if line.split.count != 3
            rescue
              p "Error processing line: #{line}"
              next
            end

            time, trash, word = line.split

            # time = previous_time if time.to_f < previous_time.to_f
            raise "review times on #{wrd}" if time.to_f < previous_time.to_f

            out << "#{previous_time}  #{time}  #{word.upcase} \n"

            previous_time = time
          else
            sharp_spotted = line.include? "#\n"
          end
        end
      end
      
      if File.read(lab_name).strip.empty?
        raise "#{lab_name} doesn't have content"
      end
    end
  end

  #  for TIMIT corpus wrds format (measured in sec/10000)
  #  Example:
  # -------------------------------------------------
  #   2260 5320 she
  #   5320 8944 had
  #   8420 9733 your
  #   9733 14173 dark
  #   14173 18160 suit
  #   18160 20138 in
  #   20138 25939 greasy
  #   25939 31080 wash
  #   32080 37138 water
  #   37138 40175 all
  #   40175 45400 year
  # ------------------------------------------------

  def from_wrds_to_labs_timit
    Log.write "Extracting labs for TIMIT Corpus"
    foreach_file_in @wrds_folder, 'wrd' do |wrd|

      name = File.basename(wrd).sub(".wrd","")
      folder = File.dirname(wrd)

      File.open("#{@save_labs_in}/#{name}.lab",'w') do |out|
        File.open(wrd).each do |line|
          begin
            line = clean_encode_problems(line)
            next if line.split.count != 3
          rescue
            p "Error processing line: #{line}"
            next
          end

          start, endd, word = line.split

          out << "#{start.to_f/10000}  #{endd.to_f/10000}  #{word.upcase} \n"
        end
      end
    end
  end

end
