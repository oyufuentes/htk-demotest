class Grammar
  def initialize(kind, options)
    @kind = kind
    @options = options
    require_and_load(options, :grammar, :grammar_wdnet)
  end

  def generate_wordnet
    # HParseAlternative.parse(@grammar, @grammar_wdnet)
    HTK.hparse grammar: @grammar, grammar_wdnet: @grammar_wdnet
  end

  def create
    send "create_for_#{@kind}"
  end

  def create_for_merged
    require_and_load(@options, :keywords)

    keywords = @keywords.map &:upcase

    File.open(@grammar, 'w') do |f|
      f << "$keyword = #{keywords.join ' | '}; \n"
      f << "( SENT-START {FILLER | $keyword} SENT-END ) \n"
    end

  end

  def create_for_recognizer
    require_and_load(@options, :all_words)

    all_words = @all_words.map &:upcase

    File.open(@grammar, 'w') do |f|
      f << "$all_words = #{all_words.join ' | '}; \n"
      f << "( SENT-START {$all_words} SENT-END ) \n"
    end

  end

  def set_filler_probability(probability)
    log_prob = Math.log(probability)

    text = File.read(@grammar_wdnet)

    filler_line = text.split("\n").find {|x| x.include? "FILLER"}
    #I=0   W=FILLER
    node_number = /I=(\d+).*/.match(filler_line)[1]

    prob = "%.2f" % log_prob
    new_grammar = text.gsub("E=#{node_number}","E=#{node_number}    l=#{prob}" )

    File.open(@grammar_wdnet, "w") do |f|
      f.write(new_grammar)
    end

    #  By default, all arcs are equally likely. However, the optional field l=x can be used to attach the log
    #     transition probability x to an arc. For example, if the word “but” was twice as likely as “bit”, the arcs numbered 1
    #     and 2 in the last example could be changed to
    #
    #     J=1 S=4 E=2 l=-1.1
    #     J=2 S=4 E=3 l=-0.4
    #
    #  Here the probabilities have been normalised to sum to 1, however, this is not necessary.
    #     The recogniser simply adds the scaled log probability to the path score and hence it
    #     can be regarded as an additive word transition penalty.
  end


end
