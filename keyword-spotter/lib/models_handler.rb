class ModelsHandler

  def initialize(options)
    require_and_load(options,
    :on,
    :phones_list,
    :states,
    :training_list,
    :training_words_mlf,
    :training_phones_mlf,
    :herest_scripts_path,
    :kind,
    :split_training_every,
    )

    @options = options
    @keywords = options[:keywords]
    @filler_states = options[:filler_states] || 5
    @filler_kind = options[:filler_kind] || :ergodic 
    @iteration = last_iteration
  end

  def prepare
    clean @on

    FileUtils.mkdir_p current

    flat_start_initialisation
    copy_prototype
    create_macros_file
  end

  def prepare_scripts
    # Scripts (or lists) for reestimation processes.
    Scripts.generate_herest_lists({
      from: @training_list,
      contained_on: @training_words_mlf,
      on: @herest_scripts_path,
      split_every: @split_training_every
    })
  end

  def split_training
    FileUtils.mkdir_p(current)

    herest_list = Dir.glob("#{@herest_scripts_path}*")
    count = herest_list.count

    herest_list.each_with_index do  |script, i|
      HTK.herest({
        portion: i+1,
        training_phones_mlf: @training_phones_mlf,
        phones_list: @phones_list,
        current_model: current,
        next_model: next_model,
        script: script,
      })
    end

    HTK.herest_join({
      phones_list: @phones_list,
      current_model: current,
      next_model: next_model,
    })

  end

  def fix_silences
    copy_all current, next_model

    # erease_hmm "sp"
    copy_sil_state
  end

  #private:

  def flat_start_initialisation
    #calculates global mean and covariances
    HTK.hcompv({
      current_model: current,
      training_list: @training_list
    })
  end

  def copy_prototype
    destination = "#{current}/hmmdefs"

    FileUtils.mkdir_p(File.dirname(destination))

    phoneme_prototype = Sources.prototype_for :phoneme, states: @states
    phoneme_prototype_name = File.basename(phoneme_prototype)

    filler_kind = @filler_kind == :ergodic ? :filler : :phoneme
    filler_prototype = Sources.prototype_for filler_kind, states: @filler_states
    filler_prototype_name = File.basename(filler_prototype)

    File.open(@phones_list).each_line do |phoneme|
      phoneme = phoneme.delete("\n")
      if phoneme == 'fill'
        safe_system_call "tail -n +2 #{filler_prototype} | sed 's/'#{filler_prototype_name}'/'#{phoneme}'/g' >> #{destination}"
      else
        safe_system_call "tail -n +2 #{phoneme_prototype} | sed 's/'#{phoneme_prototype_name}'/'#{phoneme}'/g' >> #{destination}"
      end
    end
  end

  def create_macros_file
    macros = "#{current}/macros"
    vfloors = "#{current}/vFloors"

    File.open(macros, "w") do |file|
      file.puts "~o"
      file.puts "<STREAMINFO> 1 39"
      file.puts "<VECSIZE> 39<NULLD><MFCC_D_A_0><DIAGC>"
      file.puts File.read(vfloors)
    end
  end

  # def erease_hmm(hmm_name)
  #   result = File.read("#{next_model}/hmmdefs").gsub(/~h "#{hmm_name}"[^~h]*~h/m,"~h").gsub(/~h "#{hmm_name}"[^~h]*$/m,"")
  #   File.open("#{next_model}/hmmdefs", 'w') do |file|
  #     file.puts result
  #   end
  # end

  def copy_all(src, dst)
    FileUtils.mkdir_p dst
    FileUtils.cp Dir["#{src}/*"].collect{|f| File.expand_path(f)}, dst
  end

  def copy_sil_state
    silhmm = false
    in_state = false
    state3 = ""
    File.open("#{next_model}/hmmdefs").each do |line|
      in_state = false if line == "<STATE> 4\n"
      (state3 << line) if silhmm && in_state

      silhmm = false if line.include? "~h"
      silhmm = true if line == "~h \"sil\"\n"

      in_state = true if line == "<STATE> 3\n" && silhmm
    end

    File.open("#{next_model}/hmmdefs", 'a') do |file|
      file.puts File.read(Sources.sp_model).gsub("SIL_STATE_3", state3.chomp)
    end

  end

  def tie_silence_models
    @phones_list.write "sp\n", append: true
    @phones_list.sort_file

    FileUtils.mkdir_p next_model
    HTK.hhed_tie({
      current_model: current,
      next_model: next_model,
      phones_list: @phones_list
    })
  end

  def increment_iteration
    @iteration += 1
  end

  def current
    model_path(@on, @iteration)
  end

  def next_model
    model_path(@on, @iteration + 1)
  end

  def iteration
    @iteration
  end

  def last_iteration
    raise "directory '#{@on}' does not exists" unless File.exists? @on
    models = Dir["#{@on}*"]
    return 0 if models.empty?
    models.human_sort.select {|x| Dir[x + "/*"].map(&:basename).include? "hmmdefs"}.last.basename.scan(/\d+/).first.to_i
  end

end

