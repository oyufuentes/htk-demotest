class Statistics
  def self.from_mfccs_list(list)
    info = {}
    
    list.each do |l|
      lab = l.gsub(".mfc", ".lab")
      lab.each do |line|
        start, finish, word = line.split
        word.gsub!(/[^a-zA-Z\n]*/,"")
        info[word] = Hash.new(0) unless info[word]
        info[word][:count] += 1
        info[word][:time] += (finish.to_f - start.to_f)
      end
    end
     
    info.each{ |_,val| 
	val[:avg] = (val[:time]/val[:count]).round(3)
	val[:time] = val[:time].round 3 
     }.sort_by{|x,y| y[:count]}
    info
  end
end
