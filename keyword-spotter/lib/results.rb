class Results

  def initialize(params)
    require_and_load(params, :results_mlf)
  end

  def compare_with(reference_mlf, options = {})
    results = Results.results_hash_for(@results_mlf, options[:log_prob_tolerance], options[:only_for])
    references = Results.results_hash_for(reference_mlf, nil, options[:only_for])
    
    validate results, references, @results_mlf, reference_mlf
    counts = Results.calculate_results(results, references)
    Results.add_fom counts, options[:total_tests_time] if options[:total_tests_time] 
    counts = Results.flat_counts counts, references
    counts
  end

  def validate(results, references, results_mlf, reference_mlf)
    files = results.keys | references.keys
    unless files == (results.keys & references.keys)
      missing_on_results = references.keys - results.keys
      missing_on_reference = results.keys - references.keys
      unless missing_on_results.empty? || $quiet
        puts "MISSING ON RESULTS MLF #{results_mlf}: \n"
        missing_on_results.each {|x| puts x}
      end 
      unless missing_on_reference.empty? || $quiet
        puts "MISSING ON REFERENCE MLF #{reference_mlf}: \n" 
        missing_on_reference.each {|x| puts x} 
      end
      raise "Files missing on reference/results mlf"
    end
  end

  def self.calculate_results(results, references)
    counts = {}
    files = references.keys
    	
    files.each do |file|
      references[file].each do |ref|
        counts[ref[:word]] ||= {}
        counts[ref[:word]][:total] ||= 0
        counts[ref[:word]][:total] += 1
      end
    end

    files.each do |file|
      results[file].each do |res|
        res_word = res[:word]
        counts[res_word] ||= {}
        counts[res_word][:total] ||= 0
        counts[res_word][:insertions] ||= 0
        counts[res_word][:substitutions] ||= 0
        counts[res_word][:fa] ||= []

        time_found = false
        references[file].each do |ref|
          mid_point = (ref[:start] + ref[:end])/2.0
          if res[:start] < mid_point && res[:end] > mid_point
            time_found = true
            if res[:word] == ref[:word]
              counts[res_word][:hits] ||= []
              counts[res_word][:hits] << res[:log_prob]
            else
              counts[res_word][:fa] << res[:log_prob]
              counts[res_word][:substitutions] += 1
            end
            break
          end
        end
        unless time_found
          counts[res_word][:fa] << res[:log_prob]
          counts[res_word][:insertions] += 1
        end
      end
    end
    counts
  end

  def self.add_fom(counts, total_tests_time)
    counts.each do |word, probs|
      roc_list = roc_list_for(probs).map{|p,d| d}
      counts[word][:fom] = calculate_fom(roc_list, probs[:total], total_tests_time)
    end
  end

  def self.calculate_fom(roc_list, total_count, total_tests_time)
    roc = roc_curve(roc_list, total_count, true) 
    hits_percentaje = (roc_list.count {|x| x == :hit})/ total_count.to_f
    t = total_tests_time
    n = ((10 * t) - 0.5).ceil
    a = (10 * t) - n 
    if roc.size < (n + 1)
      p = roc.concat [hits_percentaje]*(n + 1 - roc.size)
    else
      p = roc[0..n]
    end
    return ((1.0/(10*t)) * (p[0..n-1].sum + a*p[n])).round 3
  end

  def self.roc_list_for(probs)
    roc_list = []
    (probs[:hits] || []).each do |prob|
      roc_list << [prob,:hit]
    end

    (probs[:fa] || []).each do |prob|
      roc_list << [prob,:fa]
    end

    roc_list.sort_by! {|x| x[0]}.reverse!
    roc_list
  end

  def self.flat_counts(counts, references)
    files = references.keys
    res = {}
    counts.each do |name, probs|
      res[name] = Hash.new(0)
      res[name][:hits] = probs[:hits] ? probs[:hits].count : 0
      res[name][:fa] = probs[:fa] ? probs[:fa].count : 0
      res[name][:fom] = probs[:fom]
      res[name][:substitutions] = probs[:substitutions]
      res[name][:insertions] = probs[:insertions]
      res[name][:total] = probs[:total]
    end
    res
  end

  def self.summarize(comparition)
    res = Hash.new(0)
    values = comparition.values.select {|v| v[:total] > 0}

    res[:hits] = (values.map {|x| x[:hits]}).sum
    res[:fa] = (values.map {|x| x[:fa]}).sum
    # res[:insertions] = (values.map {|x| x[:insertions]}).sum 
    # res[:substitutions] = (values.map {|x| x[:substitutions]}).sum
    res[:total] = (values.map {|x| x[:total]}).sum
    res[:fom] = (values.map {|x| x[:fom]}).average
    res[:weigthed_fom] = (values.map {|x| x[:fom]}).weighted_average(values.map {|x| x[:total]})
    #res[:rate_hits_fa] = (res[:hits].to_f / res[:fa]).round 3
    res[:rate_hits_total] = (res[:hits].to_f / res[:total]).round 3
    #res[:fom_by_rates] = (res[:fom] * res[:rate_hits_total] * res[:rate_hits_fa]).round 3 if res[:fom]
    res
  end

  def self.results_hash_for(mlf, log_prob_tolerance = nil, only_for = nil, continue_on = {})
    res_hash = continue_on
    file = ""
    mlf.each_with_index do |l, i|
      next if i == 0
      l = l.strip
      next if l == "."
      next if l.include? "!MLF!"
      if l.start_with? "\""
        file = l.gsub(".rec", ".lab")
        res_hash[file] ||= []
      else
        start, endd, word, log_prob = l.split
        next if log_prob_tolerance && log_prob && log_prob.to_f < log_prob_tolerance
        raise "invalid line(#{i+1}) in #{mlf}" unless word
        next if (word.downcase == "filler")
        next if only_for && only_for.downcase != word.downcase
        res_hash[file] << {word: word, start: start.to_f, end: endd.to_f, log_prob: log_prob.to_f}
      end
    end
    res_hash
  end

  def draw_timelines
    res = []
    each_result do |file_name, content|
      content.split("\n").each do |each|
        next if each.strip.empty?
        start, endd, word = each.split
        start = ((start.to_f)/10000) #Milliseconds
        endd = ((endd.to_f)/10000) #Milliseconds
        res << "['#{File.basename file_name[0..-2]}', '#{word}', #{start}, #{endd} ]"
      end
    end
    "[" + res.join(",") + "]"
    # [ 'Willow Room',   'Advanced Google Charts',     new Date(0,0,0,16,30,0), new Date(0,0,0,18,0,0) ]
  end

  def each_result
    File.read(@results).scan(/(\"[^\"]*\")(.[^\.]*)\./m) {|file_name, content| yield(file_name, content)}
  end

  def self.roc(results_mlf, reference_mlf, keyword, options)
    results = results_hash_for(results_mlf, nil, keyword)
    references = results_hash_for(reference_mlf, nil, keyword)
    data = calculate_results(results, references)
    roc_list = roc_list_for(data[keyword]).map{|p,d| d}
    binding.pry
    roc_curve(roc_list, data[keyword][:total], options[:fom])
  end

  def self.roc_curve(roc_list, total, only_false_alarms_rates = false)
    roc = []
    hits = 0
    roc = [0] unless only_false_alarms_rates
    roc_list.each_with_index do |detection, i|
      if detection == :hit
        hits += 1
      end
      percentaje = (hits / total.to_f).round 3
      roc << percentaje unless detection == :hit && only_false_alarms_rates
    end
    roc
  end

end

