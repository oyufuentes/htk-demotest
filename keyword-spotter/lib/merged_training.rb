require 'training'

class MergedTraining < Training
  def initialize(options)
    super(options)
    require_and_load(options,
      :keywords,
      :dictionary,
      :states,
      :filler_states,
      :filler_kind,
      :fill_using,
    )
    options[:words] = @keywords
  end

  def kind  
    :merged
  end

end



