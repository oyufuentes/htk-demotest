require 'testing.rb'

class MergedTesting < Testing

  def initialize(options)
    require_and_load(options,
      :model,
      :phones_list,
      :phones_dict,
    )
    @filler_probability = options[:filler_probability]
    super

    unless options[:keywords_sample]
      @keywords = File.read(@words_sorted).split("\n").map(&:downcase).reject {|k| k == "filler"}
    else
      @keywords = Keywords.sample_data(options[:keywords_sample].to_i).map(&:downcase)
    end
  end

  def start
    prepare_grammar
    test
    prepare_reference_file
  end

  def prepare_grammar
    grammar = Grammar.new(:merged, {
      grammar: @grammar,
      grammar_wdnet: @grammar_wdnet,
      keywords: @keywords
    })
    grammar.create
    grammar.generate_wordnet
    grammar.set_filler_probability @filler_probability if @filler_probability
  end

  def prepare_reference_file
    transcriptions_handler = TranscriptionsHandler.new
    transcriptions_handler.generate_master_label_files(:merged, {
      words_mlf: @reference_mlf,
      files_list: @testing_list,
      keywords: @keywords,
      fill_using: :intervals,
      interval_in_seconds: 0.5
    })
  end

end
