require 'matrix'
require 'waveinfo'

class Common
  def self.drop(training_folder, tests_folder, dropbox)
    testing_dropbox_folder = "#{dropbox}/tests"
    FileUtils.mkdir_p testing_dropbox_folder
    Dir.glob("#{tests_folder}/*").each do |dir|
      name = File.basename dir
      puts "#{dir} => #{testing_dropbox_folder}" unless $quiet
      FileUtils.cp_r dir, testing_dropbox_folder
    end

    taining_folder_dropbox = "#{dropbox}/trained"
    Dir.glob("#{training_folder}/*").each do |dir|
      name = File.basename dir
      drop = "#{taining_folder_dropbox}/#{name}"

      puts "#{dir} => #{drop}" unless $quiet
      FileUtils.mkdir_p drop
      FileUtils.cp_r "#{dir}/data/", drop
      FileUtils.cp_r "#{dir}/mlfs/", drop
      FileUtils.cp_r "#{dir}/scripts/", drop if File.exists? "#{dir}/scripts/"
      FileUtils.mkdir_p "#{drop}/models"
      (Dir.glob("#{dir}/models/*0") + Dir.glob("#{dir}/models/*5")).each do |model|
        FileUtils.cp_r model, "#{drop}/models/"
      end
    end
  end

  def self.total_time_in_seconds(list)
    duration = 0
    list.each do |test_file|
      # ./data/mfccs/f1ajrlp1.mfc
      duration += WaveInfo.new(test_file.gsub("mfccs", "corpus").gsub(".mfc", ".wav")).duration
    end
    duration
  end
end

def foreach_file_in(src, extensions ='wav', recursively = true, mixing = false, &block)
  src += "/" unless src[-1] == "/"
  files = Dir.glob("#{src}#{'**/' if recursively}*.{#{extensions}}")
  if mixing
    groups = files.group_by{|x| File.basename(x)[0..1]}
    puts "mixing #{groups.keys} files"
    values = groups.values
    m = Matrix.build(values.size, values.map {|x| x.size}.max) {|row, col| values[row][col]}
    files = m.transpose.to_a.flatten.select {|x| x}
  end
  files.each(&block)
end

def foreach_file_with_index_in(src, extensions ='wav', recursively = true, &block)
  src += "/" unless src[-1] == "/"
  Dir.glob("#{src}#{'**/' if recursively}*.{#{extensions}}").each_with_index(&block)
end

def foreach_in_list(file, &block)
  File.read(file).split.each(&block)
end

def keywords
  keywords_from(Dictionary.file_for :words_sorted)
end

def keywords_from(file)
  list = File.read(file).split()
end

def set_keywords(keywords)
  keywords = keywords.map &:upcase
  keywords = keywords.sort

  File.open(Dictionary.file_for(:words_sorted), "w") do |f|
    f.write keywords.join "\n"
  end
end


def filtered_training_list(training_list_file, training_words_mlf)
  # Returns the list of files contained on training_words_mlf that have the correspondent .lab
  training_list = File.read(training_list_file).split
  train_words_from_mlf = File.read(training_words_mlf)
  training_list.select {|x| train_words_from_mlf.include? x.gsub(".mfc",".lab")}
end

def filtered_tests_list(tests_list, tests_words_mlf)
  # Returns the list of files contained on tests_words_mlf that have the correspondent .lab
  tests_list = File.read(tests_list).split
  tests_words_from_mlf = File.read(tests_words_mlf)
  tests_list.select {|x| tests_words_from_mlf.include? x.gsub(".mfc",".lab")}
end

def safe_system_call(command)
  res = `#{command}`
  puts command if $print_commands
  Log.write command
  Log.write res

  unless $?.success?
    binding.pry if $debugging
    raise "Something went wrong running\n #{command} \n"
  end
  res
end

def fetch_or_raise(options, option)
  raise "missing the options hash" unless options.is_a? Hash
  res = options[option]
  raise "\nThe option: :#{option} is required. \nOptions: #{options}\n" if res.nil?
  res
end

def require_and_load(options, *required)
  raise "missing the options hash in require_and_load command" unless options.is_a? Hash
  required.each do |opt|
    value = fetch_or_raise(options, opt)
    instance_variable_set("@" + opt.to_s, value)
  end
end

def clean(dir)
  puts "Cleaning #{dir}" unless $quiet
  FileUtils.rm_rf dir
  FileUtils.mkdir_p dir
end

def logging_execution_time(start_msg = nil)
  puts start_msg if start_msg unless $quiet
  Log.write start_msg if start_msg

  took = Benchmark.realtime do
    yield
  end

  time = Time.at(took).gmtime.strftime('%R:%S')

  msg = "Total time: #{time}"
  Log.write msg
  puts msg unless $quiet

  time
end

def model_path(folder, iteration)
  folder += "/" unless folder.end_with? "/"
  "#{folder}hmm_#{iteration}"
end

def merge(file1, file2, merge, options = {})
  File.open(merge,'w') do |f|
    t1 = File.read(file1)
    t2 = File.read(file2)

    t1 = t1.split("\n").drop(options[:skip_from_first]).join "\n" if options[:skip_from_first]
    t2 = t2.split("\n").drop(options[:skip_from_second]).join "\n" if options[:skip_from_second]

    f << t1
    f << t2
  end
end

def clean_encode_problems(str)
  str.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
end
