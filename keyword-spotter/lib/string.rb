class String
  def sort_file
    new_array = File.readlines(self).uniq.sort
    File.open(self,"w") do |file|
      file.puts new_array
    end
  end

  def cd(folder)
    self.end_with?("/") ? (self + folder) : (self + "/" + folder)
  end 

  def basename
    File.basename self
  end

  def dirname
    File.dirname self
  end

  def write(text, options = {})
    text += "\n" unless text.end_with? "\n"

    if options[:append] && File.exists?(self)
      File.write self, text, File.size(self), mode: 'a'
    else
      File.write self, text
    end
  end

  def add(line)
    self << "#{line}\n"
  end

  def take(n)
    lines = File.readlines self
    lines = lines.take n
    self.write lines.join
  end

  def each_with_index
    f = File.open(self, "r")
    f.each_with_index do |line, i|
      yield line.gsub("\n",""), i
    end
    f.close
  end

  def save_as(filename)
    File.write(filename, self)
  end

  def each
    f = File.open(self, "r")
    f.each do |line|
      yield line.gsub("\n","")
    end
    f.close
  end

  def all
    res = []
    self.each do |x|
      res << x
    end 
    res
  end

  def count_lines
    `wc -l "#{self}"`.scan(/\d+/).first
  end

  def readlines_from(from, to = nil, complete_line = true, *exclude_from_if_contains)
    lines = []
    from_found = false
    to_found = false
    self.each do |l|
      to_found = to && from_found && (to_found || (complete_line ? l == to : l.include?(to)))
      from_found = from_found || matches(l, from, complete_line, exclude_from_if_contains)
      lines << l if from_found && (!to || !to_found)
    end
    lines
  end

  private
  
  def matches(l, from, complete, exclude)
    if complete
      return l == from unless exclude && exclude.any? {|x| l.include?(x)}
    else
      return l.include?(from) unless exclude && exclude.any? {|x| l.include?(x)}
    end
  end
end

