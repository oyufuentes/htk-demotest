class Log

  def self.start(folder = "./log/", options = {})
    @logging_folder = folder
    puts "Logging started (default: #{folder})" unless options[:quiet]
  end

  def self.on(folder)
    @logging_folder = folder
    puts "Logging on #{@logging_folder}"
  end

  def self.stop
    @logging_file = nil
    puts "logging disabled"
  end

  def self.log(str)
    write(str)
  end

  def self.write(str)
    if @logging_folder
      FileUtils.mkdir_p @logging_folder
      logging_file = "#{@logging_folder}/log"
      logging_file.write "#{Time.now}: #{str}", append: true
    elsif !$testing
      puts str
    end
  end
end
