class Scripts

  def self.generate_for_hcopy(files_list, outputs_folder, script_file)
    FileUtils.mkdir_p File.dirname(script_file)
    outputs_folder += "/" unless outputs_folder.end_with? "/"
    FileUtils.mkdir_p(outputs_folder)

    File.open(script_file, "w") do |script|
      File.open(files_list).each do |wav|
        wav.strip!
        mfc = outputs_folder + File.basename(wav).gsub(".wav",".mfc")
        script.puts "#{wav}  #{mfc}"
      end
    end

  end

  def self.generate_list(list, source, extension = 'wav')
    FileUtils.mkdir_p File.dirname(list)
    File.open list, "w" do |l|
      foreach_file_in source, extension do |file|
        l.puts file
      end
    end
  end

  def self.generate_lists(options)
    require_and_load(options, :mfccs_list, :training_list, :testing_list, :partition)
    raise "partition must be between 1 and 5 (6th partition is reserved)" unless @partition >= 1 && @partition < 6

    #generate train-test lists with 4 of 6 for training, 1 of 6 to test, keeping 1 of 6 untouchable. 
    #partition (example: 4 means, if the mfccs_list is divided by 6, the 4th part is for testing, all the other data, to train, except the untouchable part)

    FileUtils.mkdir_p File.dirname(@training_list)
    FileUtils.mkdir_p File.dirname(@testing_list)

    lines = File.readlines @mfccs_list
    lines.sort! 
    File.open(@training_list, "w") do |train|
      lines.each_with_index do |file, i|
        train.write(file) if i%6 != (@partition - 1) && i%6 != 5
      end
    end

    File.open(@testing_list, "w") do |tests|
      lines.each_with_index do |file, i|
        tests.write(file) if i%6 == (@partition - 1)
      end
    end

  end

  def self.generate_results_lists(mfccs_list, dst)
    FileUtils.mkdir_p dst

    merged_folder = dst.cd "merged"
    FileUtils.mkdir_p merged_folder

    by_speaker_folder = dst.cd "by_speaker"
    FileUtils.mkdir_p by_speaker_folder


    # Merged results
    training_list = merged_folder.cd "training.list"
    testing_list = merged_folder.cd "testing.list"

    lines = File.readlines mfccs_list
    lines.sort! 
    File.open(training_list, "w") do |train|
      lines.each_with_index do |file, i|
        train.write(file) if i%6 != 5
      end
    end

    File.open(testing_list, "w") do |tests|
      lines.each_with_index do |file, i|
        tests.write(file) if i%6 == 5
      end
    end


    # By Speaker results
    speaker_groups = lines.group_by {|l| l.basename[0..1]}
    speaker_groups.each do |spkid, files|
      file_name = by_speaker_folder.cd "testing.#{spkid}.list"
      File.open(file_name, "w") do |spk_test|
        files.each do |file|
          spk_test.write(file)
        end
      end
      
      file_name = by_speaker_folder.cd "training.#{spkid}.list"
      File.open(file_name, "w") do |spk_train|
        speaker_groups.select {|x| x != spkid}.values.flatten.each do |file|
          spk_train.write(file)
        end
      end
    end
    
  end


  def self.generate_herest_lists(options)
    # # fills HERest*.script files with lines with the following sintax:
    # #./Data/Train/train1-7.mfc
    training_list = fetch_or_raise(options, :from)
    training_words_mlf = fetch_or_raise(options, :contained_on)
    herest_scripts_path = fetch_or_raise(options, :on)
    split_every = fetch_or_raise(options, :split_every)

    clean herest_scripts_path

    filtered_training_list(training_list, training_words_mlf).each_slice(split_every).to_a.each_with_index do |mfcs_group, i|
      File.open("#{herest_scripts_path}/HERest-#{i}.script", "w") do |io|
        mfcs_group.each do |mfc|
          io.write("#{mfc}\n")
        end
      end
    end
  end

end


