class Sources
  @prototypes = {
    filler: :ergodic_prototype,
    phoneme: :left_right_prototype,
  }

  def self.dictionary
    "sources/dictionary/dict"
  end

  def self.sp_model
    "sources/prototypes/sp_model"
  end

  def self.prototype_for(kind, options={})
    source = "sources/prototypes/#{@prototypes[kind]}"
    source << "_" << options[:states].to_s << "_states" if options[:states]

    raise "The prototype file is missing: #{source}" unless File.exists? source
    source
  end
end
