class Testing
  
  def self.new(options)
    super
  end

  def initialize(options)
    require_and_load(options,
    :grammar, :grammar_wdnet, :transition_penality, :likehoods_scaling,
    :macros, :results,
    :testing_list, :reference_mlf, :words_sorted
    )

    @options = options

    results_dir = File.dirname @results
    FileUtils.mkdir_p(results_dir)

    
    FileUtils.cp @testing_list, "#{results_dir}/files.list"

    @testing_list = "#{results_dir}/files.list"
    @testing_list.take options[:samples].to_i if options[:samples]
  end

  def test
    HTK.hvite({
      macros: @macros,
      hmmdefs: @model,
      testing_list: @testing_list,
      output_mlf: @results,
      grammar_wdnet: @grammar_wdnet,
      phones_dict: @phones_dict,
      phones_list: @phones_list,
      transition_penalty: @transition_penality,
      likehoods_scaling: @likehoods_scaling,
      prunning: @options[:prunning]
    })
    FileUtils.cp @words_sorted, @results.dirname unless @words_sorted.dirname == @results.dirname
    FileUtils.cp @model, File.dirname(@results)

  end

end
