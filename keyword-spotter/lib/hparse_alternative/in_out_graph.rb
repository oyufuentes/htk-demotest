require 'parser.rb'
require 'set.rb'

class Node
  @@nodes = Set.new

  attr_reader :name, :number
  attr_accessor :to

  def self.reset
    @@nodes = Set.new
  end

  def self.nodes
    @@nodes
  end

  def initialize(name = "!NULL")
    @name = name
    @to = []
    @@nodes << self
  end

  def to_s
    "#{@name} #{@number}"
  end

  def nodes
    Set.new << self
  end

  def connect_with(object)
    if object.is_a? Node
      @to << object
    else
      @to << object.entry_point
    end
  end
end

class InOutGraph
  attr_reader :entry_point, :end_point

  def to_s
    "#{@entry_point} "
  end

  def initialize(input, output)
    if input.is_a? Node
      @entry_point = input
    else
      @entry_point = input.entry_point
    end

    if output.is_a? Node
      @end_point = output
    else
      @end_point = output.end_point
    end
  end

  def connect_with(object)
    @end_point.connect_with object
    self
  end

  def nodes
    Node.nodes
  end

  def print_slf
    str = ""

    nodes = Node.nodes
    nodes_to_ids = {}

    nodes.each_with_index do |n, i|
      nodes_to_ids[n] = i
    end

    str << "# List nodes: I=node-number, W=word\n"
    nodes_to_ids.each do |node, id|
      str << "I=#{id}   W=#{node.name}" << "\n"
    end

    str << "# List arcs: J=arc-number, S=start-node, E=end-node\n"
    j = 0
    nodes_to_ids.each do |node, id|
      node.to.each do |to|
        str << "J=#{j}     S=#{id}    E=#{nodes_to_ids[to]}" << "\n"
        j+=1
      end
    end
    str = "# Define size of network: N=num nodes and L=num arcs \nN=#{nodes_to_ids.size}  L=#{j}\n" + str
    return str
  end

  def print_dot
    # digraph graphname {
    #   a -> b -> c;
    #   b -> d;
    #}

    str = "digraph graphname { \n"

    nodes = Node.nodes
    nodes_to_ids = {}

    nodes.each_with_index do |n, i|
      nodes_to_ids[n] = i
    end

    nodes_to_ids.each do |node, id|
      node.to.each do |to|
        name = node.name.delete("!-")
        to_name = to.name.delete("!-")
        str << "#{name}_#{id} -> #{to_name}_#{nodes_to_ids[to]};" << "\n"
      end
    end
    str << "}"
    return str
  end
end
