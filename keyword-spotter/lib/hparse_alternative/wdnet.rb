require 'in_out_graph.rb'
require 'parser.rb'

class WDNet
  def self.build_from(parse_tree)
    self.new.build(parse_tree)
  end

  def build(tree)
    graph = build_graph(tree)
    return add_init_and_end_to graph
  end

  def build_graph(tree)
    case tree
    when Name 
      #Build a graph with only one node as input and output. 
      node = Node.new tree.name
      return InOutGraph.new node, node
    when Concat
      #Build a graph joining all the subgraphs in order. 
      sub_graphs = tree.commands.map {|c| build_graph c}
      return build_for_concat sub_graphs
    when Optional
      sub_graph = build_graph tree.command
      return build_for_optional sub_graph
    when ZeroOrMore
      sub_graph = build_graph tree.command
      return build_for_cero_or_more sub_graph
    when OneOrMore
      sub_graph = build_graph tree.command
      return build_for_one_or_more sub_graph
    when Or
      sub_graphs = tree.commands.map {|c| build_graph c}
      return build_for_ors sub_graphs
    end
  end

  def build_for_concat(sub_graphs)
    for i in 0..(sub_graphs.size-2) do
      sub_graphs[i].connect_with sub_graphs[i+1]
    end
    return InOutGraph.new sub_graphs[0], sub_graphs[-1]
  end

  def build_for_optional(sub_graph)
    first_node = Node.new
    last_node = Node.new
    sub_graph.connect_with last_node
    first_node.connect_with sub_graph
    first_node.connect_with last_node
    return InOutGraph.new first_node, last_node
  end

  def build_for_cero_or_more(sub_graph)
    first_node = Node.new
    last_node = Node.new
    sub_graph.connect_with last_node
    first_node.connect_with sub_graph
    sub_graph.connect_with first_node
    first_node.connect_with last_node
    return InOutGraph.new first_node, last_node
  end

  def build_for_one_or_more(sub_graph)
    first_node = Node.new
    last_node = Node.new
    go_back_node = Node.new
    sub_graph.connect_with last_node
    sub_graph.connect_with go_back_node
    go_back_node.connect_with sub_graph
    first_node.connect_with sub_graph
    return InOutGraph.new first_node, last_node
  end

  def build_for_ors(sub_graphs)
    first_node = Node.new
    last_node = Node.new 
    sub_graphs.each do |sub| 
      first_node.connect_with sub
      sub.connect_with last_node
    end
    return InOutGraph.new first_node, last_node
  end

  def add_init_and_end_to(graph)
    last_node = Node.new
    graph.connect_with last_node
    first_node = Node.new
    first_node.connect_with graph
    return InOutGraph.new first_node, last_node
  end
end
