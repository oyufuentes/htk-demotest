require 'parser.rb'
require 'wdnet.rb'

class HParseAlternative
  def self.parse(grammar_file = "./Grammar/grammar", grammar_wdnet)

    grammar = File.read(grammar_file).gsub!("\n","")

    match = /\((.*)\)/.match(grammar)
    content = match[1]

    #Replace variables on the content.
    while content.include? "$" do
      grammar.scan(/(\$[^;]*)=([^;]*);/).each do |asignation|
        var_name = asignation[0].strip
        var_value = asignation[1].strip
        content.gsub!(var_name, var_value)
      end
    end

    parsed_content = Parser.parse(content)

    Node.reset
    graph = WDNet.build_from(parsed_content)

    File.open(grammar_wdnet,'w') do |f|
      f.write(graph.print_slf)
    end

    File.open(grammar_wdnet+".dot",'w') do |f|
      f.write(graph.print_dot)
    end


  end

  # Parser that converts from a subset of the grammar HTK with the following alternatives:
  #| denotes alternatives
  #[ ] encloses options
  #{ } denotes zero or more repetitions
  #< > denotes one or more repetitions
  #to SLF format.

end
