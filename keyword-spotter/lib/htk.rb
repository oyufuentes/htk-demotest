require 'hparse_alternative'

class HTK
  class << self
    def print_ouput_flag
      # $quiet ? "" : "-A"
      ""
    end

    def logging_flag
      $quiet ? "-T 0" : "-T 1"
    end

    def binary_flag
      # $testing ? "" : "-B"
      "" # the problem with -B is when models have to be merged...
    end

    def hcopy(script)
      safe_system_call "HCopy #{print_ouput_flag} #{logging_flag} -C #{Configs.for :hcopy} -S #{script}"
    end

    def hparse(params)
      require_and_load(params, :grammar, :grammar_wdnet)
      safe_system_call "HParse #{print_ouput_flag} #{logging_flag} #{@grammar} #{@grammar_wdnet}"
    end

    def hparse_alternative(params)
      require_and_load(params, :grammar, :grammar_wdnet)
      HParseAlternative.parse @grammar, @grammar_wdnet
    end

    def hhed_tie(params)
      require_and_load(params, :current_model, :next_model, :phones_list)
      safe_system_call "HHEd #{print_ouput_flag} #{logging_flag} -H #{@current_model}/macros -H #{@current_model}/hmmdefs -M #{@next_model} #{Configs.for :hhed, :tie} #{@phones_list}"
    end

    def hhed_mixtures(params)
      require_and_load(params, :current_model, :next_model, :phones_list, :mixtures_config)
      safe_system_call "HHEd #{print_ouput_flag} #{logging_flag} -H #{@current_model}/macros -H #{@current_model}/hmmdefs -M #{@next_model} #{@mixtures_config} #{@phones_list}"
    end

    def hcompv(params)
      require_and_load(params, :current_model, :training_list)

      prototype = Sources.prototype_for(:filler, states: 5)
      safe_system_call "HCompV #{print_ouput_flag} #{logging_flag} -C #{Configs.for :hcompv} -f 0.01 -m -S #{@training_list} -M #{@current_model} #{prototype}"
    end

    def hled(params)
      require_and_load(params, :phones_dict, :words_mlf, :phones_mlf, :kind, :include_sp)
      case @kind
      when :filler
        config = Configs.for :hled, "filler"
      else
        config = @include_sp ? Configs.for(:hled) : Configs.for(:hled, "no_sp")
      end
      safe_system_call "HLEd #{print_ouput_flag} #{logging_flag} -d #{@phones_dict} -i #{@phones_mlf} #{config} #{@words_mlf}"
      #-l '*' to generalize routes
    end

    def herest(params)
      require_and_load(params, :portion, :training_phones_mlf, :phones_list, :current_model, :next_model, :script)
      FileUtils.mkdir_p(@next_model)
      safe_system_call "HERest #{print_ouput_flag} #{logging_flag} #{binary_flag} -p #{@portion} -C #{Configs.for :herest} -I #{@training_phones_mlf} -t 250.0 150.0 1000.0 -S #{@script} -H #{@current_model}/macros -H #{@current_model}/hmmdefs -M #{@next_model} #{@phones_list}"
    end

    def herest_join(params)
      require_and_load(params, :phones_list, :current_model, :next_model)
      safe_system_call "HERest #{print_ouput_flag} #{logging_flag} #{binary_flag} -H #{@current_model}/macros -H #{@current_model}/hmmdefs -M #{@next_model} -p 0 #{@phones_list} #{@next_model}/*.acc"
    end

    def hdman(params)
      require_and_load(params, :words_sorted, :phones_list, :phones_dict, :dictionary)
      safe_system_call("HDMan #{print_ouput_flag} #{logging_flag} -g #{Configs.for :hdman} -l #{@phones_dict + '.log'} -m -w #{@words_sorted} -n #{@phones_list} #{@phones_dict} #{@dictionary}")
    end

    def hvite(params)
      require_and_load(params, :macros, :hmmdefs, :testing_list, :output_mlf, :grammar_wdnet, :phones_dict, :phones_list, :transition_penalty, :likehoods_scaling)
      transition_penalty = "-p #{@transition_penalty}"
      likehoods_scaling = "-s #{@likehoods_scaling}" #Scales the l=-10 on the wordnet for example.
      prunning = params[:prunning] ? "-t #{params[:prunning]}" : ""   
      safe_system_call("HVite #{print_ouput_flag} #{logging_flag} #{prunning} #{transition_penalty} #{likehoods_scaling} -H #{@macros} -H #{@hmmdefs} -C #{Configs.for :hvite} -S #{@testing_list} -i #{@output_mlf} -w #{@grammar_wdnet} #{@phones_dict} #{@phones_list}")
    end

    def hresults(params)
      require_and_load(params, :reference, :words_list, :results)
      #-T 00020 for detailed results
      safe_system_call("HResults #{print_ouput_flag} -w -I #{@reference} #{@words_list} #{@results}")
    end

  end
end


# HCompV:

# This program will calculate the global mean and covariance of a set of training data.
# It is primarily used to initialise the parameters of a HMM such that all component
# means and all covariances are set equal to the global data mean and covariance.

# When training large model sets from limited data, setting a floor is often necessary to prevent
# variances being badly underestimated through lack of data. One way of doing this is to define a variance
# macro called varFloorN (or VFloor) where N is the stream index.
# HCompV can also be used to create these variance floor macros with values equal to a specified
# fraction of the global variance.

# Another application of HCompV is the estimation of mean and variance vectors for use in cluster-based
# mean and variance normalisation schemes. Given a list of utterances and a speaker pattern HCompV
# will estimate a mean and a variance for each speaker.

# HCompV [options] [hmm] trainFiles ...

# -f f Create variance floor macros with values equal to f times the global variance. One macro is created
# for each input stream and the output is stored in a file called vFloors

# -m The covariances of the output HMM are always updated however updating the means must
# be specifically requested. When this option is set, HCompV updates all the HMM component means with
# the sample mean computed from the training files.

# Example: HCompV -C Configs/HCompV.config -f 0.01 -m -S Scripts/HCompV.script -M Models/hmm0 Models/prototype


# HLEd:

# the HLEd command expands the Word Level Transcriptions to Phone Level Transcriptions
# i.e. replace each word with its phonemes, and put the result in a new Phone Level Master Label File
# This is done by reviewing each word in the MLF file, and looking up the phones that make up that word in the dict file.
# Then, outputs the result in a file (phones_mlf)


# HERest

# The -t option sets the pruning thresholds to be used during training.
# Pruning limits the range of state alignments that the forward-backward algorithm includes
# in its summation and it can reduce the amount of computation required by an order of magnitude.
# For most training files, a very tight pruning threshold can be set, however,
# some training files will provide poorer acoustic matching

# and in consequence a wider pruning beam is needed. HERest deals with this by having an
# auto-incrementing pruning threshold. In the above example, pruning is normally 250.0.
# If re-estimation fails on any particular file, the threshold is increased by 150.0 and the file is reprocessed.
# This is repeated until either the file is successfully processed or the pruning limit of 1000.0 is exceeded.

# At this point it is safe to assume that there is a serious problem with the training file and hence the fault should
# be fixed (typically it will be an incorrect transcription) or the training file should be discarded.

# test with: 1000.0 1000.0 10000.0

# HVite

# HVite -s 100 -p $TRANSITION_PENALITY -A -o S -H $MACROS -H $HMMDEFS -S $FILTERED_TESTS_LIST -i $OUTPUT_MLF -w $GRAMMAR_WNET $MERGED_PHONES_DICT $MERGED_PHONES_LIST
# #-r 1.0 (pronunciation score scale factor)
# #-s 100 (logprob mutiplier) (for example multiplies the l=-10, weights options in the grammar network)
# #-o ST (omit scores and times)
# #-p 0.0 (penalty)

# In order to use HVite effectively and efficiently, it is important to set appropriate values for its pruning thresholds and the language model scaling parameters. 
# The main pruning beam is set by the -t option. Some experimentation will be necessary to determine appropriate levels but around 250.0 is usually a reasonable starting point. 
# Word-end pruning (-v) and the maximum model limit (-u) can also be set if required, but these are not mandatory and their effectiveness will depend greatly on the task.
# The relative levels of insertion and deletion errors can be controlled by scaling the language model likelihoods using the -s option and adding a fixed penalty using the -p option. 
# For example, setting -s 10.0 -p -20.0 would mean that every language model log probability x would be converted to 10x ° 20 before being added to the tokens emitted from the corresponding word-end node. 
# As an extreme example, setting -p 100.0 caused the digit recogniser above to output

# HResults
#-e ??? FILLER  to ignore filler in the results.
#If required the standard time unit of 1 hour as used in the above definition of FOM can be changed using the -u option.
#http://speech.fit.vutbr.cz/software/kwsviewer-interactive-viewer-keyword-spotting-output

# HDMan
# For HTK to be able to compile your speech audio and transcriptions into an Acoustic Model, 
# it requires a phonetically balanced Pronunciation Dictionnary with at the very least 30-40 
# 'sentences' of 8-10 words each. If your Grammar has fewer sentences/words than this 
# (as we do in this tutorial), or if your grammar in not  phonetically balanced 
# (if some phonemes only occur one or two times) then we need to add additional words to make 
# sure we have 3-5 occurences of each phoneme in our Pronunciation Dictionnary.

# HHEd
# Speech recognition systems will often have distinct models for silence and short pauses.
# A silence model sil may have the normal 3 state topology whereas a short pause model may have just a single state.
# To avoid the two models competing with each other,
# the sp model state can be tied to the centre state of the sil model thus
# TI "silst" { sp.state[2], sil.state[3] }

