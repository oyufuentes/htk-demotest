require 'benchmark'

class Training

  def self.train(kind, options)
    case kind
    when :filler
      training = FillerTraining.new(options)
    when :keywords
      training = KeywordsTraining.new(options)
    when :merged
      training = MergedTraining.new(options)
    when :recognizer
      training = RecognizerTraining.new(options)
    else
      raise "Unknown training kind: #{kind}"
    end
    start(training)
  end

  def self.start(training)
    training.prepare_for_training unless training.continues?

    training.prepare_scripts

    jobs = training.fetch_jobs

    training.execute jobs

    training
  end

  def initialize(options)
    require_and_load(options,
    :copy_data_on,
    :herest_scripts_path,
    :increment_mixtures_by,
    :increment_mixtures_every,
    :mfccs_folder,
    :on,
    :phones_dict,
    :phones_list,
    :reestimations,
    :split_training_every,
    :states,
    :testing_list,
    :training_list,
    :training_phones_mlf,
    :training_words_mlf,
    :word_as_unit,
    :words_sorted,
    )
    @options = options

    FileUtils.mkdir_p @on

    @models = ModelsHandler.new(@options.merge kind: kind)
  end

  def fetch_jobs
    jobs = [:reestimate] * @reestimations
    jobs = jobs.intercalate(:add_mixtures, @increment_mixtures_every)
    jobs.insert 3, :fix_silences if kind != :filler
    jobs.insert 4, :tie_silence_models if kind != :filler
    jobs
  end

  def continues?
    @models.iteration > 0
  end

  def execute(jobs)
    print_execution_info jobs unless $quiet

    jobs.drop(@models.iteration).each do |job|
      puts "\n#{@models.iteration+1} of #{jobs.size} (#{job.to_s.capitalize}): #{@models.current} ----> #{@models.next_model}" unless $quiet

      time = logging_execution_time do
        send(job)
      end

      save_execution_info job, time

      @models.increment_iteration
    end
  end

  def print_execution_info(jobs)
    puts "\nExecuting plan:"

    jobs.each_with_index do |job,i|
      puts "#{i+1}) #{job.to_s}"
    end

    puts "skipping: #{jobs.take(@models.iteration)}" if @models.iteration > 0
  end

  def save_execution_info(job, time)
    "#{@copy_data_on}/execution".write "#{Time.now}: #{job} (#{time}) [#{@models.current} -> #{@models.next_model}]", append: true
  end

  def add_mixtures
    mixes = Mixtures.set({
      kind: kind,
      current_model: @models.current,
      next_model: @models.next_model,
      phones_list: @phones_list,
      keywords_states: @states,
      filler_states: @filler_states || @states,
      keywords_increment_by: @increment_mixtures_by,
      filler_increment_by: @options[:increment_filler_mixtures_by] || @increment_mixtures_by,
      mixtures_config: @models.next_model + "/mixtures.config",
    })
    @mixtures = mixes[:keywords]
    @filler_mixtures  = mixes[:filler]
  end

  def prepare_for_training
    Dictionary.prepare_for(kind, @options)
    copy_data
    prepare_transcriptions
    @models.prepare
  end

  def prepare_scripts
    @models.prepare_scripts
  end

  def reestimate
    @models.split_training
    Dir["#{@models.current}/*.acc"].each {|x| FileUtils.rm x}
  end

  def fix_silences
    @models.fix_silences
  end

  def tie_silence_models
    @models.tie_silence_models
    FileUtils.cp @training_phones_mlf, @training_phones_mlf.gsub(".mlf", ".no-sp.mlf")

    transcriptions = TranscriptionsHandler.new
    transcriptions.create_phone_level_mlf(kind, {
      phones_dict: @phones_dict, 
      phones_mlf: @training_phones_mlf, 
      words_mlf: @training_words_mlf,
      include_sp: true
    })

  end

  protected

  def prepare_transcriptions
    transcriptions_handler = TranscriptionsHandler.new

    transcriptions_handler.generate_master_label_files(kind, {
      phones_mlf: @training_phones_mlf,
      words_mlf: @training_words_mlf,
      files_list: @training_list,
      phones_dict: @phones_dict,
      keywords: @keywords,
      all_words: @all_words,
      fill_using: @fill_using,
      interval_in_seconds: @options[:filler_every]
    })

  end
  
  def copy_data
    FileUtils.mkdir_p @copy_data_on

    FileUtils.cp @testing_list, "#{@copy_data_on}/testing.list"
    FileUtils.cp @training_list, "#{@copy_data_on}/training.list"
  end

end
