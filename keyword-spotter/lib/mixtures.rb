class Mixtures

  def self.set(options)
    require_and_load(options, :kind, :mixtures_config, :current_model, :next_model, :phones_list)
    FileUtils.mkdir_p @next_model
    FileUtils.rm @mixtures_config if File.exists? @mixtures_config

    current_model_hmmdef = @current_model + "/hmmdefs"
    next_model_hmmdefs = @next_model + "/hmmdefs"

    unless @kind == :keywords || @kind == :recognizer 
      require_and_load(options, :filler_states, :filler_increment_by)
      filler_mixtures = process_mixtures_config(
        {
          config: @mixtures_config,
          states: @filler_states,
          increment_by: @filler_increment_by,
          kind: :filler,
          hmmdefs: current_model_hmmdef      })
    end

    unless @kind == :filler
      require_and_load(options, :keywords_states, :keywords_increment_by)
      keywords_mixtures = process_mixtures_config(
        {
          config: @mixtures_config,
          states: @keywords_states,
          increment_by: @keywords_increment_by,
          kind: :keywords,
          hmmdefs: current_model_hmmdef,
          phonemes: @phones_list.all
      })
    end

    HTK.hhed_mixtures(options)

    unless @kind == :filler
      result_keywords = on(next_model_hmmdefs, :keywords)
      if result_keywords != keywords_mixtures
        p "Couldn't update keyword mixtures, keeping the current number of mixtures (#{result_keywords})"
        result_keywords = keywords_mixtures
      end
    end

    unless @kind == :keywords || @kind == :recognizer
      result_filler = on(next_model_hmmdefs, :filler)
      if result_filler != filler_mixtures
        p "Couldn't update filler mixtures, keeping the current number of mixtures (#{result_filler})"
        result_filler = filler_mixtures
      end
    end

    {keywords: result_keywords, filler: result_filler}
  end

  def self.process_mixtures_config(options)
    mixtures_increment = fetch_operation(options[:increment_by])
    previous_mixtures = File.exists?(options[:hmmdefs]) ? on(options[:hmmdefs], options[:kind]) : 1
    mixtures = mixtures_increment.call previous_mixtures
    states_in = "[2-#{options[:states]-1}]"
    phoneme_symbol = options[:kind] == :filler ? "fill" : "(#{options[:phonemes].without('fill').join ','})"
    options[:config].write "MU #{mixtures} {#{phoneme_symbol}.state#{states_in}.mix}\n", append: true
    return mixtures
  end

  def self.on(hmmdefs, kind)
    case kind
    when :filler
      extract_from hmmdefs.readlines_from("~h \"fill\"", "~h", false)
    when :keywords
      extract_from hmmdefs.readlines_from("~h", "~h", false, "fill", "sp") 
    end      
  end

  def self.extract_from(model_lines)
    return nil if model_lines.empty?
    nummixes = model_lines.find {|x| x.include? "NUMMIXES"}
    return 1 unless nummixes
    match = nummixes.match(/<NUMMIXES> (\d+)/)
    match[1].to_i
  end

  def self.fetch_operation(operation)
    match = operation.match /(.)(\d+)/
    case match[1]
    when "+"
      Proc.new { |x| x + match[2].to_i } 
    when "*","x" 
      Proc.new { |x| x * match[2].to_i }
    else 
      raise "error parsing #{operation}, expected '+3' or '*4' (or the same, 'x4') for example"
    end

  end
  #   # One final point with regard to multiple mixture component distributions is that all HTK tools
  #   # ignore mixture components whose weights fall below a threshold value called MINMIX (defined in HModel.h).
  #   # Such mixture components are called defunct. Defunct mixture components can be prevented by setting the -w option
  #   # in HERest so that all mixture weights are floored to some level above MINMIX. If mixture weights are allowed
  #   # to fall below MINMIX then the corresponding Gaussian parameters will not be written out when the model
  #   # containing that component is saved. It is possible to recover from this, however,
  #   # since the MU command will replace defunct mixtures before performing any requested mixture component increment.
  # }

end
