require 'common.rb'

class Corpus
  def self.extract(src, dst, options = {})
    extension_to_find = options[:extension_to_find] || "wav"
    formats = options[:include] || ["sph", "wrd", "wav"]

    FileUtils.mkdir_p dst

    foreach_file_in src, extension_to_find do |file|
      file_without_extension = file.gsub(".#{extension_to_find}","")

      if options[:copy_if_all]
        not_found = []
        formats.each do |ext|
          to_copy = "#{file_without_extension}.#{ext}"
          not_found << to_copy unless File.exists? to_copy
        end
        raise "files not found: #{not_found}" unless not_found.empty?
      end

      formats.each do |ext|
        to_copy = "#{file_without_extension}.#{ext}"
        next unless File.exists?(to_copy)

        yield to_copy, dst if block_given?
        FileUtils.cp to_copy, dst unless block_given?
      end
    end
  end

end
