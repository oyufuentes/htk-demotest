require 'training'

class RecognizerTraining < Training
  def initialize(options)
    super(options)
    require_and_load(options,
      :all_words,
      :dictionary
    )

    options[:words] = @all_words
  end

  def kind  
    :recognizer
  end

end



