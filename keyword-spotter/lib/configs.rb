require 'common.rb'

class Configs
  @config_files = {
    hcopy: "WAVtoMFCC",
    hcompv: "HTKtoMFCC",
    herest: "HTKtoMFCC",
    hhed: "HHEd", 
    hled: "HLEd",
    hdman: "HDMan",
    hvite: "HVite"
  }

  def self.for(command, suffix = "")
    "sources/configs/#{@config_files[command]}#{suffix}.config"
  end
end
