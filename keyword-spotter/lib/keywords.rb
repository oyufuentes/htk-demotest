class Keywords
  def self.choose(params)
    require_and_load(params,
    :wrds_source,
    :how_many,
    :keyword_min_length,
    :with_at_least_n_apparitions,
    :corpus
    )
    @allow_out_of_dictionary = params[:allow_out_of_dictionary]
    @dictionary = fetch_or_raise(params, :dictionary) unless @allow_out_of_dictionary

    wrds_beggining_mark = "#" if @corpus == :boston

    words = self.fetch_from_wrds(@wrds_source, @keyword_min_length, wrds_beggining_mark)
    most_common = words.counts_down
    most_common.select! {|word_with_count| word_with_count[1] > @with_at_least_n_apparitions}
    unless @allow_out_of_dictionary
      found = filter_present_on_dictionary(most_common.map &:first)
      
      most_common = most_common.select {|x| found.include? x.first }.compact.uniq.sort.reverse
    end
    keywords = most_common.take(@how_many)
    raise "not enought keywowrds found\n found: #{keywords}" if keywords.size < @how_many

    puts "Keywords: [#{keywords.map{|x| x.join ":"}.join ' - '}]" unless $quiet

    keywords.map &:first
  end

  def self.sample_data(sample_number)
    case sample_number
    when 0
      return ["according","companies","government","governor","massachusetts","melnicove","nineteen","officials","thousand","yesterday"]
    when 1
      return ["about", "boston", "by", "city", "committee", "congress" , "government", "hundred" , "massachusetts", "official", "percent" , "president", "program", "public", "seven", "state", "thousand", "time", "year", "yesterday"]
    when 2
      return ["and", "are", "as", "at", "be", "but", "by", "for", "in", "is", "it", "of", "on", "says", "state", "that", "the", "to", "will", "with"]
    when 3 
      return ["according", "administration", "association", "committee", "democratic", "democrats", "education", "environmental", "government", "lawmakers", "legislature", "massachusetts", "melnicove", "officials", "political", "president", "republican", "springfield", "superintendent", "yesterday"]
    when 4
      return ["according", "administration", "attorney", "business", "committee", "companies", "computer", "democratic", "education", "environmental", "expected", "gellerman", "government", "governor", "hennessy", "legislature", "massachusetts", "melnicove", "nineteen", "officials", "political", "president", "programs", "proposal", "republican", "services", "students", "thousand", "university", "yesterday"]
    when 5
      return ["according", "administration", "association", "attorney", "business", "campaign", "children", "commentator", "committee", "companies", "computer", "democratic", "education", "environmental", "expected", "gellerman", "government", "governor", "hennessy", "industry", "legislature", "massachusetts", "melnicove", "national", "nineteen", "officials", "political", "president", "problems", "programs", "proposal", "republican", "research", "residents", "scientists", "services", "students", "thousand", "university", "yesterday"]
    when 6
      return ["according", "association", "commentator", "community", "democratic", "democrats", "department", "education", "environmental", "gellerman", "lawmakers", "legislature", "meanwhile", "president", "representative", "republican", "residents", "scientists", "secretary", "superintendent", "university"]
    else
      raise "no sample data for sample_number #{sample_number}"
    end
  end

  def self.fetch_from_wrds(where, min_word_size = 1, beginning_mark = nil)
    word_list = []
    foreach_file_in where, 'wrd' do |f|
      begin
        
        str = File.read(f)
        str = str.gsub("\n","")[/#{beginning_mark}.*/] if beginning_mark
        words = str
          .split(' ')
          .map{|x| x.delete('^a-zA-Z').downcase}
          .select {|x| x.size >= min_word_size}

        word_list.concat words
      rescue
      end
    end
    word_list
  end

  def self.all_words(params)
    require_and_load(params, :wrds_source, :dictionary)

    words = self.fetch_from_wrds(@wrds_source).uniq
    words = filter_present_on_dictionary(words)

    words
  end

  def self.filter_present_on_dictionary(words)
    dict = Hash[File.read(@dictionary).split("\n").map {|l| [l.split[0].downcase, true]}]
    words.select {|x| dict[x.downcase]}
  end

end

# For HTK to be able to compile your speech audio and transcriptions into an Acoustic Model,
# it requires a phonetically balanced Pronunciation Dictionnary with at the very least 30-40
# 'sentences' of 8-10 words each. If your Grammar has fewer sentences/words than this
# (as we do in this tutorial), or if your grammar in not  phonetically balanced
# (if some phonemes only occur one or two times) then we need to add additional words to make
# sure we have 3-5 occurences of each phoneme in our Pronunciation Dictionnary.

