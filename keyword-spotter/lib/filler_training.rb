require 'training'

class FillerTraining < Training
  def initialize(options)
    super(options)
    require_and_load(options,
      :fill_using,
    )
  end

  def kind
    :filler
  end

end
