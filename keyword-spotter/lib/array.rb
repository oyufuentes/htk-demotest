class Array
  def intercalate(elem, every)
    len = self.length
    current = every
    while current < len
      insert current, elem
      len += 1
      current = current + every + 1
    end
    self
  end

  def counts
    inject( Hash.new(0) ) do |hash,element|
      hash[ element ] +=1
      hash
    end
  end

  def counts_down
    counts.sort_by{ |k,v| -v }
  end

  def average
    return nil if self.include? nil
    (self.sum.to_f / self.count).round 3
  end

  def weighted_average(counts)
    return nil if self.include? nil
    res = 0
    self.each_with_index do |v,i|
      res += v * counts[i]
    end
    (res.to_f / counts.sum).round 3
  end

  def sum
    return nil if self.include? nil
    if block_given?
      self.reduce(0) {|res, x| (yield x) + res}
    else
      self.reduce {|x, res| x + res}
    end
  end

  def as_table(options = {})
    title = options[:title]
    sort_by = options[:sort_by]
    ascending =  options[:ascending] || !options[:descending]
    summary_row = options[:summary]
    group_by = options[:group_by]

    # ↑ ↓
    headings = self.map(&:keys).flatten.uniq
    rows = []

    self.human_sort_by!(sort_by.to_sym) if sort_by
    self.reverse! unless ascending
    self.each do |hash|
      rows << headings.map { |k| hash[k] } 
    end
    
    if group_by
      groups = rows.group_by {|r| r[0].scan(/#{group_by}_(\d+)/).last}
      rows = []
      groups.each do |group, data|
        rows += data
        rows << :separator
      end
    end

    if summary_row
      rows << :separator
      rows << (headings.map {|h| summary_row[h]}).insert(0,"total")
    end

    headings = headings.map(&:to_s).map {|heading| heading == sort_by ? heading + (ascending ? " (↑)" : " (↓)") : heading} if sort_by 
    headings = headings.insert 0, ""
    rows.each_with_index do |r,i|
      break if i == rows.count - 1 && summary_row
      next if r == :separator
      r.insert 0, i+1
    end

    TableProxy.new(options, Terminal::Table.new(title: title, headings: headings, rows: rows))
  end

  def without(elem)
    arr = self
    self.delete elem
    arr
  end

  def human_sort
    sort_by { |item| item.kind_of?(Numeric) ? item : item.to_s.split(/(\d+)/).map { |e| [e.to_i, e] } }
  end

  def human_sort_by!(field)
    sort_by! { |item| item.kind_of?(Numeric) ? item : item[field].to_s.split(/(\d+)/).map { |e| [e.to_i, e] } }
  end
end

class TableProxy
    instance_methods.each { |m| undef_method m unless m =~ /(^__|^send$|^object_id$)/ }

    def initialize(options, table)
      @options = options
      @target = table
    end
    
    def to_s
      if @options[:latex]
        puts LatexHelper.table(target)
      else
        target.to_s
      end
    end

    protected
    def method_missing(name, *args, &block)
      target.send(name, *args, &block)
    end

    def target
      @target ||= []
    end
end
