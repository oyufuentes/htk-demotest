require 'testing.rb'

class SplitTesting < Testing

  def initialize(options)
    require_and_load(options,
      :keywords_model, :filler_model, :model,
      :keywords_phones_list, :filler_phones_list, :phones_list,
      :keywords_phones_dict, :filler_phones_dict, :phones_dict,
      :keywords_words_sorted, :filler_words_sorted, :words_sorted
    )

    @filler_probability = options[:filler_probability]
    super
    
  end

  def start
    merge_keywords_and_filler_files
    prepare_grammar
    test
    prepare_reference_file
  end

  def prepare_grammar(keywords = nil)
    grammar = Grammar.new(:merged, {
      grammar: @grammar,
      grammar_wdnet: @grammar_wdnet,
      keywords: @keywords || keywords
    })
    grammar.create
    grammar.generate_wordnet
    grammar.set_filler_probability @filler_probability if @filler_probability
  end

  def merge_keywords_and_filler_files
    [@model, @phones_dict, @phones_list, @words_sorted].each do |f|
      FileUtils.mkdir_p(File.dirname f)
    end

    merge @filler_model, @keywords_model, @model, skip_from_second: 3
    merge @keywords_phones_list, @filler_phones_list, @phones_list
    merge @keywords_phones_dict, @filler_phones_dict, @phones_dict
    merge @keywords_words_sorted, @filler_words_sorted, @words_sorted

    [@phones_dict, @phones_list, @words_sorted].each do |f|
      f.sort_file
    end

    @keywords = File.read(@words_sorted).split("\n").map(&:downcase).reject {|k| k == "filler"}

  end

  def prepare_reference_file
    transcriptions_handler = TranscriptionsHandler.new
    transcriptions_handler.generate_master_label_files(:merged, {
      words_mlf: @reference_mlf,
      files_list: @testing_list,
      keywords: @keywords,
      fill_using: :intervals,
      interval_in_seconds: 0.5
    })
  end

end
