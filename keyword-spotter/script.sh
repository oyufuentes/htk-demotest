thor ks:test --test-name m1_hmm_120 ./trained/m1/models/hmm_120/ ./trained/m1/data/ --kind merged
thor ks:test --test-name m2_hmm_120 ./trained/m2/models/hmm_120/ ./trained/m2/data/ --kind merged
thor ks:test --test-name m3_hmm_120 ./trained/m3/models/hmm_120/ ./trained/m3/data/ --kind merged
thor ks:test --test-name m4_hmm_120 ./trained/m4/models/hmm_120/ ./trained/m4/data/ --kind merged

thor ks:test --test-name f1_hmm_120 ./trained/f1/models/hmm_120/ ./trained/f1/data/ --kind merged
thor ks:test --test-name f2_hmm_120 ./trained/f2/models/hmm_120/ ./trained/f2/data/ --kind merged
thor ks:test --test-name f3_hmm_120 ./trained/f3/models/hmm_120/ ./trained/f3/data/ --kind merged
