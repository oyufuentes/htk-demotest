require 'spec_helper'

describe Statistics do
  it "should count keywords correctly" do
    "./Testing/test1.lab".write "1 2 keyA\n 2 3 keyB\n3 4 keyA\n"
    "./Testing/test2.lab".write "1 2 keyB\n 2 3 keyA\n3 4 keyC\n"
    "./Testing/mfccs_list".write "./Testing/test1.mfc\n./Testing/test2.mfc\n"

    stats = Statistics.from_mfccs_list("./Testing/mfccs_list")
    stats["keyA"][:count].should eq(3)
    stats["keyA"][:time].should eq("1.000")

    stats["keyB"][:count].should eq(2)
    stats["keyC"][:count].should eq(1)

  end

end



