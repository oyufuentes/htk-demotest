require 'spec_helper'

describe Scripts do
  before :each do
    make_fake_files 10, 'wav', "./Testing/wavs/"
    make_fake_files 10, 'wrd', "./Testing/wavs/"
  end

  it "creates a file list with the right amount of lines" do
    Scripts.generate_list("./Testing/list/my_list.txt", "./Testing/wavs/")

    `wc -l < ./Testing/list/my_list.txt`.strip.to_i.should eq(10)
  end

  it "generates script file for hcopy" do
    script = "./Testing/Scripts/hcopy.script"
    Scripts.generate_list("./Testing/list/my_list.txt", "./Testing/wavs/")
    Scripts.generate_for_hcopy "./Testing/list/my_list.txt", "./Testing/ble/", script

    script.should include_content("./Testing/wavs/1.wav")
    script.should include_content("./Testing/ble/1.mfc")


    `wc -l < ./Testing/Scripts/hcopy.script`.strip.to_i.should eq(10)

  end

  it "creates cross validation files correctly" do
    "./Testing/mfccs.list".write "1.mfc\n2.mfc\n3.mfc\n4.mfc\n5.mfc\n6.mfc\n7.mfc\n8.mfc\n"

    Scripts.generate_lists({
      mfccs_list: "./Testing/mfccs.list",
      training_list: "./Testing/training.list",
      testing_list: "./Testing/testing.list",
      partition: 3,
    })

    "./Testing/training.list".should have_content("1.mfc\n2.mfc\n4.mfc\n5.mfc\n7.mfc\n8.mfc\n")
    "./Testing/testing.list".should have_content("3.mfc\n")
  end

  it "creates cross validation files sorting the files first" do
    "./Testing/mfccs.list".write "2.mfc\n1.mfc\n5.mfc\n3.mfc\n8.mfc\n6.mfc\n7.mfc\n4.mfc\n"

    Scripts.generate_lists({
      mfccs_list: "./Testing/mfccs.list",
      training_list: "./Testing/training.list",
      testing_list: "./Testing/testing.list",
      partition: 3,
    })

    "./Testing/training.list".should have_content("1.mfc\n2.mfc\n4.mfc\n5.mfc\n7.mfc\n8.mfc\n")
    "./Testing/testing.list".should have_content("3.mfc\n")
  end

  it "raises exception if partition 6 is used (it should be reserved)" do
    "./Testing/mfccs.list".write "1.mfc\n2.mfc\n3.mfc\n4.mfc\n5.mfc\n6.mfc\n7.mfc\n8.mfc\n"

    expect {
      Scripts.generate_lists({
        mfccs_list: "./Testing/mfccs.list",
        training_list: "./Testing/training.list",
        testing_list: "./Testing/testing.list",
        partition: 6,
      })
    }.to raise_error

  end

end


