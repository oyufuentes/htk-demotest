require 'spec_helper'

describe Dictionary do
  let(:words_sorted){"./Testing/dictionary/words-sorted.list"}
  let(:phones_dict){"./Testing/dictionary/phones.dict"}
  let(:phones_list){"./Testing/dictionary/phones.list"}

  it "creates dictionary files for filler" do
    Dictionary.prepare_for(:filler, {
      words_sorted: words_sorted,
      phones_dict: phones_dict,
      phones_list: phones_list,
    })

    words_sorted.should have_content("FILLER\n")
    phones_list.should have_content("fill\n")
    phones_dict.should include_content("FILLER")
    phones_dict.should include_content("fill")

  end

  it "creates dictionary files for keywords" do
    keywords = ["MASSACHUSSETS", "Boston", "security"]

    Dictionary.prepare_for(:keywords, {
      words_sorted: words_sorted,
      words: keywords,
      phones_dict: phones_dict,
      phones_list: phones_list,
      dictionary: "sources/dictionary/dict",
    })

    words_sorted.should include_content("MASSACHUSSETS")
    words_sorted.should include_content("BOSTON")
    words_sorted.should include_content("SECURITY")

    phones_list.should include_content("b\n")
    phones_list.should include_content("aa\n")
    phones_list.should include_content("s\n")
    phones_list.should include_content("t\n")
    phones_list.should include_content("ah\n")
    phones_list.should include_content("n\n")

    phones_dict.should include_content("b aa s t ah n sp")

    phones_list.should_not include_content("sp\n")
    phones_list.should include_content("sil\n")
  end

  it "creates dictionary files for keywords using words as unit" do
    keywords = ["MASSACHUSSETS", "Boston", "security"]

    Dictionary.prepare_for(:keywords, {
      words_sorted: words_sorted,
      words: keywords,
      phones_dict: phones_dict,
      phones_list: phones_list,
      dictionary: "sources/dictionary/dict",
      word_as_unit: true,
    })

    words_sorted.should include_content("MASSACHUSSETS")
    words_sorted.should include_content("BOSTON")
    words_sorted.should include_content("SECURITY")

    phones_list.should include_content("massachussets\n")
    phones_list.should include_content("boston\n")
    phones_list.should include_content("security\n")

    phones_dict.should include_content("MASSACHUSSETS\tmassachussets")
    phones_dict.should include_content("BOSTON\tboston")
    phones_dict.should include_content("SECURITY\tsecurity")

  end
end


