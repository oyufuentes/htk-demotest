require 'spec_helper'

describe Keywords do

  it "choose top 10 keywords" do
    most_common_keywords = Keywords.choose({
      wrds_source: "./spec/data/wavs/",
      how_many: 10,
      keyword_min_length: 5,
      with_at_least_n_apparitions: 1,
      allow_out_of_dictionary: true,
      corpus: :boston,
    })

    most_common_keywords.should include("court")
    most_common_keywords.should include("hennessy")
  end

  it "choose top 10 keywords with length greater or equal than 8" do
    most_common_keywords = Keywords.choose({
      wrds_source: "./spec/data/wavs/",
      how_many: 10,
      keyword_min_length: 8,
      with_at_least_n_apparitions: 1,
      allow_out_of_dictionary: true,
      corpus: :boston,
    })

    most_common_keywords.should_not include("court")
    most_common_keywords.should include("hennessy")
  end

  it "choose top 2 keywords present on the word to phonemes dictionary" do
    most_common_keywords = Keywords.choose({
      wrds_source: "./spec/data/wavs/",
      how_many: 3,
      keyword_min_length: 5,
      with_at_least_n_apparitions: 1,
      dictionary: "./spec/data/dictionary/dict",
      corpus: :boston,
    })

    most_common_keywords.should include("court")
    most_common_keywords.should include("hennessy")
    most_common_keywords.should_not include("dukakis")
    most_common_keywords.should include("governor")
    most_common_keywords.count("governor").should eq(1)

    i1 = most_common_keywords.index("hennessy")
    i2 = most_common_keywords.index("governor")
    (i1 < i2).should be_true
  end

  it "choose all words if present on the word to phonemes dictionary" do
    all = Keywords.all_words({
      wrds_source: "./spec/data/wavs/",
      dictionary: "./spec/data/dictionary/dict",
    })

    all.should include("court")
    all.should include("governor")
    all.should include("hennessy")
    all.count.should be(3)
  end


end





