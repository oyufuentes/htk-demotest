require 'spec_helper'

describe Grammar do

  it "can create simple keywords grammar" do
    grammar = Grammar.new(:merged, {
      grammar: "./Testing/grammar",
      grammar_wdnet: "./Testing/grammar.wordnet",
      keywords: %w(key1 Key2 KEY3),
    })

    grammar.create
    "./Testing/grammar".should include_content("$keyword = KEY1 | KEY2 | KEY3;")
    "./Testing/grammar".should include_content("( SENT-START {FILLER | $keyword} SENT-END )")

    grammar.generate_wordnet
    "./Testing/grammar.wordnet".should include_content("W=KEY1")
    "./Testing/grammar.wordnet".should include_content("W=KEY2")
    "./Testing/grammar.wordnet".should include_content("W=KEY3")
    "./Testing/grammar.wordnet".should include_content("W=FILLER")
      
    grammar.set_filler_probability(0.3)
    "./Testing/grammar.wordnet".should include_content("l=-1.20")

  end

end
