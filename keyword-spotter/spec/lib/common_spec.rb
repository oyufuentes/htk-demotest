require 'spec_helper'

describe Common do
  it "can send info to dropbox folder" do
    FileUtils.mkdir_p "./Testing/trained/"
    FileUtils.mkdir_p "./Testing/trained/ex1/"
    FileUtils.mkdir_p "./Testing/trained/ex1/mlfs"
    FileUtils.mkdir_p "./Testing/trained/ex1/data"
    FileUtils.mkdir_p "./Testing/trained/ex1/models"
    FileUtils.mkdir_p "./Testing/trained/ex1/models/hmm0"
    FileUtils.mkdir_p "./Testing/trained/ex1/models/hmm10"
    FileUtils.mkdir_p "./Testing/trained/ex1/models/hmm11"
    FileUtils.mkdir_p "./Testing/trained/ex1/models/hmm15"

    FileUtils.mkdir_p "./Testing/trained/ex2/"
    FileUtils.mkdir_p "./Testing/trained/ex2/mlfs"
    FileUtils.mkdir_p "./Testing/trained/ex2/data"
    FileUtils.mkdir_p "./Testing/trained/ex2/models"
    FileUtils.mkdir_p "./Testing/trained/ex2/models/hmm1"
    FileUtils.mkdir_p "./Testing/trained/ex2/models/hmm11"
    FileUtils.mkdir_p "./Testing/trained/ex2/models/hmm10"


    FileUtils.mkdir_p "./Testing/tests/"
    FileUtils.mkdir_p "./Testing/tests/t1/"
    FileUtils.mkdir_p "./Testing/tests/t1/a"
    FileUtils.mkdir_p "./Testing/tests/t1/b"
    FileUtils.mkdir_p "./Testing/tests/t1/c"



    Common.drop("./Testing/trained/", "./Testing/tests/", "./Testing/drop/")

    "./Testing/drop/trained/".should exist
    "./Testing/drop/trained/".should exist
    "./Testing/drop/trained/".should exist
    "./Testing/drop/trained/".should exist
    "./Testing/drop/trained/".should exist
    "./Testing/drop/trained/".should exist
    "./Testing/drop/trained/".should exist

    "./Testing/drop/trained/".should exist
    "./Testing/drop/trained/ex1/".should exist
    "./Testing/drop/trained/ex1/mlfs".should exist
    "./Testing/drop/trained/ex1/data".should exist
    "./Testing/drop/trained/ex1/models".should exist
    "./Testing/drop/trained/ex1/models/hmm0".should exist
    "./Testing/drop/trained/ex1/models/hmm10".should exist
    "./Testing/drop/trained/ex1/models/hmm15".should exist
    "./Testing/drop/trained/ex1/models/hmm11".should_not exist

    "./Testing/drop/trained/".should exist
    "./Testing/drop/trained/ex2/".should exist
    "./Testing/drop/trained/ex2/mlfs".should exist
    "./Testing/drop/trained/ex2/data".should exist
    "./Testing/drop/trained/ex2/models".should exist
    "./Testing/drop/trained/ex2/models/hmm1".should_not exist
    "./Testing/drop/trained/ex2/models/hmm10".should exist
    "./Testing/drop/trained/ex2/models/hmm11".should_not exist
  
    "./Testing/drop/tests/".should exist
    "./Testing/drop/tests/t1/".should exist
    "./Testing/drop/tests/t1/a/".should exist
    "./Testing/drop/tests/t1/b/".should exist
    "./Testing/drop/tests/t1/c/".should exist
  end

  it "measures total time correctly" do
    File.write("./Testing/list", "./spec/data/wavs/f1ajrlp1.mfc\n./spec/data/wavs/f1ajrlp2.mfc\n./spec/data/wavs/f1ajrlp3.mfc\n./spec/data/wavs/f1ajrlp4.mfc\n./spec/data/wavs/f1ajrlp5.mfc\n./spec/data/wavs/f1ajrlp6.mfc\n")
    Common.total_time_in_seconds("./Testing/list").ceil.should eq(165)
  end
end