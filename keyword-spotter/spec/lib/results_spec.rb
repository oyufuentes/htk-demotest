require 'spec_helper'

describe Results do

  it "detects missing files on detection" do
    detected = "MLF_HEADER
    \"file_name0\""
    reference = "MLF_HEADER
    \"file_name1\""

    "./Testing/detected.mlf".write detected
    "./Testing/reference.mlf".write reference

    expect {
      res = Results.new(results_mlf: "./Testing/detected.mlf").compare_with "./Testing/reference.mlf"
    }.to raise_error(/Files missing/)

  end

  it "detects missing files on reference" do
    detected = "MLF_HEADER
    \"file_name1\"
    .
    \"file_name2\"
    ."
    reference = "MLF_HEADER
    \"file_name1\"
    ."

    "./Testing/detected.mlf".write detected
    "./Testing/reference.mlf".write reference

    expect {
      res = Results.new(results_mlf: "./Testing/detected.mlf").compare_with "./Testing/reference.mlf"
    }.to raise_error(/Files missing/)

  end

  it "calculates ok when same time" do
    reference = "MLF_HEADER
    \"file_name1\"
    0 1 foo 1
    1 2 baz 1
    2 3 foo 1
    3 4 baz 1
    4 5 foo 1
    6 7 baz 1
    ."
    detected = "MLF_HEADER
    \"file_name1\"
    0 1 foo 1
    1 2 bar 1
    2 3 foo 1
    3 4 baz 1
    ."


    "./Testing/detected.mlf".write detected
    "./Testing/reference.mlf".write reference

    res = Results.new(results_mlf: "./Testing/detected.mlf").compare_with "./Testing/reference.mlf"

    # res["foo"][:corrects].should eq(2)
    # res["baz"][:corrects].should eq(1)
    # res["bar"][:substitutions].should eq(0)
    # res["foo"][:missed].should eq(1)
    # res["baz"][:missed].should eq(1)
    res["foo"][:hits].should eq(2)
    res["bar"][:fa].should eq(1)
    res["baz"][:hits].should eq(1)
  end

  it "calculates ok for real example" do
    reference = "MLF_HEADER
    \"./data/mfccs/f2bprop4\"
    0.87  0.96  SPRINGFIELD
    1.03  1.11  SUPERINTENDENT
    3.12  3.18  OFFICIALS
    3.31  3.34  ACCORDING
    4.97  5.02  SPRINGFIELD
    5.77  5.84  MELNICOVE
    ."
    
    detected = "MLF_HEADER  
    \"./data/mfccs/f2bprop4\"
    1.04 1.11 SUPERINTENDENT -5254.09
    3.12 3.18 OFFICIALS -4494.75
    3.31 3.36 ACCORDING -3523.52
    5.60 5.85 MELNICOVE -16622.43
    5.89 6.23 SPRINGFIELD -3523.52
    ."

    "./Testing/detected.mlf".write detected
    "./Testing/reference.mlf".write reference

    res = Results.new(results_mlf: "./Testing/detected.mlf").compare_with "./Testing/reference.mlf"

    res["SUPERINTENDENT"][:hits].should eq(1)
    res["OFFICIALS"][:hits].should eq(1)
    res["ACCORDING"][:hits].should eq(1)
    res["SPRINGFIELD"][:hits].should eq(0)
    res["SPRINGFIELD"][:fa].should eq(1)
    res["MELNICOVE"][:hits].should eq(1)
  end

  it "calculates ok when time ranges intersect" do
    reference = "MLF_HEADER
    \"file_name1\"
    0 1 foo 1
    1 2 baz 1
    3 4 foo 1
    4 5 baz 1
    5 6 foo 1
    6 7 baz 1
    7 8 bin 1
    10 11 bin 1
    ."

    detected = "MLF_HEADER
    \"file_name1\"
    0 1.4 foo 1
    1.4 2.3 bar 1
    2.7 3.7 foo 1
    3.7 4.6 baz 1
    9 10 ins 1
    ."

    "./Testing/detected.mlf".write detected
    "./Testing/reference.mlf".write reference

    res = Results.new(results_mlf: "./Testing/detected.mlf").compare_with "./Testing/reference.mlf"
    # res["foo"][:corrects].should eq(2)
    # res["baz"][:corrects].should eq(1)
    # res["foo"][:missed].should eq(1)
    # res["baz"][:missed].should eq(1)
    # res["baz"][:substitutions].should eq(1)
    # res["bin"][:missed].should eq(2)
    # res["bar"][:insertions].should eq(1)
    # res["ins"][:insertions].should eq(1)
    res["foo"][:hits].should eq(2)
    res["baz"][:hits].should eq(1)
    res["bar"][:fa].should eq(1)
    res["ins"][:fa].should eq(1)
  end

  it "removes fillers and words with low log prob" do
    reference = "MLF_HEADER
    \"file_name1\"
    0 1 foo 1
    1 2 baz 1
    2 3 foo 1
    3 4 baz 1
    4 5 foo 1
    6 7 baz 1
    7 9 filler 1
    ."
    detected = "MLF_HEADER
    \"file_name1\"
    0 1 foo 0.5
    1 2 bar 1
    2 3 foo 1
    3 4 baz 1
    10 11 filler 1
    ."

    "./Testing/detected.mlf".write detected
    "./Testing/reference.mlf".write reference

    res = Results.new(results_mlf: "./Testing/detected.mlf").compare_with "./Testing/reference.mlf", log_prob_tolerance: 0.6

    res["bar"][:fa].should eq(1)
    res["foo"][:hits].should eq(1)
    res["baz"][:hits].should eq(1)
    res["filler"].should be_nil
  end

  it "calculates roc for specific keyword" do
    Results.roc_curve([:hit, :hit, :fa, :fa, :hit, :hit], 10).should eq([0, 0.1, 0.2, 0.2, 0.2, 0.3, 0.4])
    Results.roc_curve([], 10).should eq([0])
  end

  it "calculates roc for specific keyword for FOM" do
    Results.roc_curve([:hit, :hit, :fa, :fa, :hit, :hit], 10, true).should eq([0.2, 0.2])
    Results.roc_curve([], 10, true).should eq([])
  end

  it "calculates fom correctly" do
    Results.calculate_fom([:hit]*10, 10, 1).should eq(1)
    Results.calculate_fom([:hit]*10, 20, 1).should eq(0.5)
    Results.calculate_fom([:hit]*5, 10, 1).should eq(0.5)

    Results.calculate_fom([:fa]*10, 10, 1).should eq(0)
    Results.calculate_fom([:fa]*10, 10, 1).should eq(0)

    Results.calculate_fom([:hit, :hit, :hit, :fa, :hit, :fa, :hit, :fa, :fa], 10, 0.2).should eq(0.35) #0.2 => 2 false alarms will be used
    Results.calculate_fom([:hit, :hit, :hit, :fa, :hit, :hit, :hit, :hit, :hit], 10, 0.3).should eq(0.633) 
    Results.calculate_fom([], 10, 0.3).should eq(0) 
    Results.calculate_fom([:hit], 10, 0.3).should eq(0.1) 

    Results.calculate_fom([:fa, :hit, :hit, :hit], 3, 0.1).should eq(0)
    Results.calculate_fom([:fa, :hit, :hit, :hit], 3, 0.2).should eq(0.5)

    Results.calculate_fom([:hit, :fa, :hit, :hit, :fa, :hit, :hit, :fa], 10, 1.5).should eq(0.46)
    Results.calculate_fom([:fa], 10, 1.5).should eq(0)
  end

end


