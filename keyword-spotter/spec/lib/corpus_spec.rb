require 'spec_helper'

describe Corpus do
  before :each do
    fake_directories_tree
  end

  it "extracts all files on corpus with existing dst" do
    Corpus.extract("./Testing/corpus/Data/", "./Testing/corpus/Destination/", extension_to_find: "wav", include: ["wav"])
    "./Testing/corpus/Destination/1.wav".should exist
  end

  it "extracts all files with selected extensions on corpus" do
    Corpus.extract("./Testing/corpus/Data/", "./Testing/corpus/Destination/", extension_to_find: "wav", include: ["wrd", "txt"])
    "./Testing/corpus/Destination/1.wav".should_not exist
    "./Testing/corpus/Destination/1.txt".should_not exist
    "./Testing/corpus/Destination/1.wrd".should exist
  end

  it "extracts all files on corpus with non-existing dst" do
    Corpus.extract("./Testing/corpus/Data/", "./Testing/corpus/NewDestination/bar/", extension_to_find: "wav", include: ["wav"])
    "./Testing/corpus/NewDestination/bar/1.wav".should exist
  end

  it "extracts all files on corpus finding with a different extension" do
    Corpus.extract("./Testing/corpus/Data/", "./Testing/corpus/Destination/", extension_to_find: "wrd", include: ["wav"])
    "./Testing/corpus/Destination/1.wav".should exist
    "./Testing/corpus/Destination/1.wrd".should_not exist
  end

  it "extracts all files on corpus finding by wrd, without restrictions" do
    Corpus.extract("./Testing/corpus/Data/", "./Testing/corpus/Destination/", extension_to_find: "wrd", include: ["wrd", "wav"])

    "./Testing/corpus/Destination/10.wav".should_not exist
    "./Testing/corpus/Destination/10.wrd".should exist

  end

  it "extracts all files on corpus using a function" do
    "./Testing/corpus/Data/1.wav".should exist

    Corpus.extract("./Testing/corpus/Data/", "./Testing/corpus/Destination/", extension_to_find: "wav", include: ["wav"]) do |file, dst|
      FileUtils.mv file, dst
    end

    "./Testing/corpus/Destination/1.wav".should exist
    "./Testing/corpus/Data/1.wav".should_not exist
  end

  it "extracts all files on corpus using a function" do
    "./Testing/corpus/Data/1.wav".should exist

    Corpus.extract("./Testing/corpus/Data/", "./Testing/corpus/Destination/", extension_to_find: "wav", include: ["wav", "wrd"]) do |file, dst|
      FileUtils.mv(file, dst) if File.extname(file) == ".wav"
      FileUtils.cp(file, dst) unless File.extname(file) == ".wav"
    end

    "./Testing/corpus/Destination/1.wav".should exist
    "./Testing/corpus/Data/1.wav".should_not exist

    "./Testing/corpus/Destination/1.wrd".should exist
    "./Testing/corpus/Data/1.wrd".should exist
  end
end

def fake_directories_tree
  FileUtils.mkdir_p("./Testing/corpus/Data")

  10.times do |t|
    File.open("./Testing/corpus/Data/#{t}.wav", 'w') do |f|
      f << t
    end
  end
  11.times do |t|
    File.open("./Testing/corpus/Data/#{t}.wrd", 'w') do |f|
      f << t
    end
  end

  FileUtils.mkdir("./Testing/corpus/Destination")
end



