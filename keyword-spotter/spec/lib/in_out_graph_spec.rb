require 'spec_helper'

describe InOutGraph do
  before :each do
    Node.reset
  end

  it "should have one node on simple graph" do
    node = Node.new
    graph = InOutGraph.new(node, node)
    graph.nodes.should include(node)
    graph.nodes.size.should eq(1)

    graph.entry_point.should eq(node)
    node.to.should eq([])
  end

  it "should have two nodes on semi simple graph" do
    node = Node.new
    node2 = Node.new

    graph = InOutGraph.new(node, node2)

    graph.nodes.should include(node)
    graph.nodes.should include(node2)

    graph.nodes.size.should eq(2)
    graph.entry_point.should eq(node)
    graph.end_point.should eq(node2)

    graph.entry_point.to.should eq(graph.end_point.to)
    graph.entry_point.to.should eq([])
  end

  it "should add nodes on graph conection" do
    node = Node.new
    node2 = Node.new
    graph0 = InOutGraph.new(node, node2)

    node3 = Node.new
    graph = graph0.connect_with(node3)

    graph.nodes.should include(node)
    graph.nodes.should include(node2)
    graph.nodes.should include(node3)

    graph.nodes.size.should eq(3)

    graph.entry_point.to.should eq([])
    node2.to.should eq([node3])
  end

  it "should add nodes on graph union" do
    node = Node.new
    node2 = Node.new
    graph0 = InOutGraph.new(node, node2)

    node3 = Node.new
    node4 = Node.new
    graph1 = InOutGraph.new(node3, node4)

    graph = graph0.connect_with(graph1)

    graph.nodes.should include(node)
    graph.nodes.should include(node2)
    graph.nodes.should include(node3)
    graph.nodes.should include(node4)

    graph.nodes.size.should eq(4)
  end

  it "should print slf for simple graph" do
    node = Node.new
    graph = InOutGraph.new(node, node)
    slf = graph.print_slf.split("\n")

    nodes_count = graph.nodes.size
    edges_count = 0

    slf.any?{|x| x.include?("N=#{nodes_count}")}.should be_true
    slf.any?{|x| x.include?("L=#{edges_count}")}.should be_true

    slf.any?{|x| x.include?("I=")}.should be_true
  end

  it "should print slf for complex graph" do
    # digraph graphname {
    #   SENTSTART_0 -> NULL_14;
    #   FILLER_1 -> NULL_13;
    #   ACCORDING_2 -> NULL_13;
    #   GOVERNOR_3 -> NULL_13;
    #   HENNESSY_4 -> NULL_13;
    #   MASSACHUSETTS_5 -> NULL_13;
    #   MELNICOVE_6 -> NULL_13;
    #   NINETEEN_7 -> NULL_13;
    #   OFFICIALS_8 -> NULL_13;
    #   STUDENTS_9 -> NULL_13;
    #   THOUSAND_10 -> NULL_13;
    #   YESTERDAY_11 -> NULL_13;
    #   NULL_12 -> FILLER_1;
    #   NULL_12 -> ACCORDING_2;
    #   NULL_12 -> GOVERNOR_3;
    #   NULL_12 -> HENNESSY_4;
    #   NULL_12 -> MASSACHUSETTS_5;
    #   NULL_12 -> MELNICOVE_6;
    #   NULL_12 -> NINETEEN_7;
    #   NULL_12 -> OFFICIALS_8;
    #   NULL_12 -> STUDENTS_9;
    #   NULL_12 -> THOUSAND_10;
    #   NULL_12 -> YESTERDAY_11;
    #   NULL_13 -> NULL_15;
    #   NULL_13 -> NULL_14;
    #   NULL_14 -> NULL_12;
    #   NULL_14 -> NULL_15;
    #   NULL_15 -> SENTEND_16;
    #   SENTEND_16 -> NULL_17;
    #   NULL_18 -> SENTSTART_0;
    # }

    node_sentstart_0 = Node.new("SENTSTART_0")
    node_filler_1 = Node.new("FILLER_1")
    node_according_2 = Node.new("ACCORDING_2")
    node_governor_3 = Node.new("GOVERNOR_3")
    node_hennessy_4 = Node.new("HENNESSY_4")
    node_massachusetts_5 = Node.new("MASSACHUSETTS_5")
    node_melnicove_6 = Node.new("MELNICOVE_6")
    node_nineteen_7 = Node.new("NINETEEN_7")
    node_officials_8 = Node.new("OFFICIALS_8")
    node_students_9 = Node.new("STUDENTS_9")
    node_thousand_10 = Node.new("THOUSAND_10")
    node_yesterday_11 = Node.new("YESTERDAY_11")
    node_null_12 = Node.new("NULL_12")
    node_null_13 = Node.new("NULL_13")
    node_null_14 = Node.new("NULL_14")
    node_null_15 = Node.new("NULL_15")
    node_sentend_16 = Node.new("SENTEND_16")
    node_null_17 = Node.new("NULL_17")
    node_null_18 = Node.new("NULL_18")

    graph0 = InOutGraph.new(node_sentstart_0, node_sentstart_0)
    graph1 = InOutGraph.new(node_filler_1, node_filler_1)
    graph2 = InOutGraph.new(node_according_2, node_according_2)
    graph3 = InOutGraph.new(node_governor_3, node_governor_3)
    graph4 = InOutGraph.new(node_hennessy_4, node_hennessy_4)
    graph5 = InOutGraph.new(node_massachusetts_5, node_massachusetts_5)
    graph6 = InOutGraph.new(node_melnicove_6, node_melnicove_6)
    graph7 = InOutGraph.new(node_nineteen_7, node_nineteen_7)
    graph8 = InOutGraph.new(node_officials_8, node_officials_8)
    graph9 = InOutGraph.new(node_students_9, node_students_9)
    graph10 = InOutGraph.new(node_thousand_10, node_thousand_10)
    graph11 = InOutGraph.new(node_yesterday_11, node_yesterday_11)
    graph12 = InOutGraph.new(node_null_12, node_null_12)
    graph13 = InOutGraph.new(node_null_13, node_null_13)
    graph14 = InOutGraph.new(node_null_14, node_null_14)
    graph15 = InOutGraph.new(node_null_15, node_null_15)
    graph16 = InOutGraph.new(node_sentend_16, node_sentend_16)
    graph17 = InOutGraph.new(node_null_17, node_null_17)
    graph18 = InOutGraph.new(node_null_18, node_null_18)

    graph = graph18.connect_with graph0
    graph = graph.connect_with graph14
    graph1.connect_with graph13
    graph2.connect_with graph13
    graph3.connect_with graph13
    graph4.connect_with graph13
    graph5.connect_with graph13
    graph6.connect_with graph13
    graph7.connect_with graph13
    graph8.connect_with graph13
    graph9.connect_with graph13
    graph10.connect_with graph13
    graph11.connect_with graph13
    graph12.connect_with graph1
    graph12.connect_with graph2
    graph12.connect_with graph3
    graph12.connect_with graph4
    graph12.connect_with graph5
    graph12.connect_with graph6
    graph12.connect_with graph7
    graph12.connect_with graph8
    graph12.connect_with graph9
    graph12.connect_with graph10
    graph12.connect_with graph11
    graph13.connect_with graph15
    graph13.connect_with graph14
    graph14.connect_with graph12
    graph14.connect_with graph15
    graph15.connect_with graph16
    graph16.connect_with graph17

    #Testing, it doesn't fails.
  end

end
