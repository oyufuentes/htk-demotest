require 'spec_helper'

describe "Boston Corpus" do
  before :each do
    FileUtils.mkdir_p("./Testing/corpus")
  end

  after :each do
    FileUtils.rm_rf("./Testing/")
  end

  it "extracts all files on corpus" do

    Corpus.extract("./spec/data/boston/", "./Testing/corpus/", extension_to_find: "wav", formats: ["wav", "wrd"]) do |file, dst|
      if File.extname(file) == ".wav"
        safe_system_call("sox #{file} #{dst}/#{File.basename(file)}")
      else
        FileUtils.cp(file, dst)
      end
    end

    "./Testing/corpus/f1as02p2.wav".should exist
    "./Testing/corpus/f1ajrlp1.wav".should exist
    "./Testing/corpus/f1as02p1.wav".should exist
    "./Testing/corpus/f1as02p1.wrd".should exist
    "./Testing/corpus/f1as01p4.wav".should exist
  end

end




