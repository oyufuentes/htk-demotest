require 'spec_helper'

describe ModelsHandler do

  before :each do
    FileUtils.mkdir_p "./Testing/training/"
    FileUtils.cp_r "./spec/data/training/", "./Testing/"
  end

  let :models do
    ModelsHandler.new({
      on: "./Testing/training/",
      phones_list: "phones_list",
      states: "states",
      training_list: "training_list",
      training_words_mlf: "training_words_mlf",
      training_phones_mlf: "training_phones_mlf",
      herest_scripts_path: "path",
      kind: "kind",
      split_training_every: 10,
    })
  end

  it "should fetch last iteration" do
    models.last_iteration.should eq(12)
  end

  it "should fetch current model folder" do
    models.current.should eq("./Testing/training/hmm_12")
  end

  it "should fetch next model folder" do
    models.next_model.should eq("./Testing/training/hmm_13")
  end

end