require 'spec_helper'

describe String do

  it "can take n lines" do
    "./Testing/example".write "1\n2\n3\n4\n"
    "./Testing/example".take 3
    "./Testing/example".should have_content("1\n2\n3\n")
  end

end
