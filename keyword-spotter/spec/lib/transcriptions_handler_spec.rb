require 'spec_helper'

describe TranscriptionsHandler do
  before :each do
    FileUtils.mkdir_p "./Testing/data/"
    FileUtils.cp_r "./spec/data/mfccs/", "./Testing/data/MFCCs/"
    FileUtils.cp_r "./spec/data/wavs/", "./Testing/data/corpus/"

    File.open("./Testing/training_list", "w") do |file|
      file << "./Testing/data/MFCCs/f1ajrlp1.mfc\n"
      file << "./Testing/data/MFCCs/f1ajrlp2.mfc\n"
      file << "./Testing/data/MFCCs/f1ajrlp3.mfc\n"
      file << "./Testing/data/MFCCs/f1ajrlp4.mfc\n"
      file << "./Testing/data/MFCCs/f1ajrlp5.mfc\n"
      file << "./Testing/data/MFCCs/f1ajrlp6.mfc\n"
    end

    File.open("./Testing/phones_dict", "w") do |file|
      file << "JUSTICE  j ah s t ah s sp\n"
      file << "NINETEEN  n ay n t iy n sp\n"
      file << "ADMINISTRATION a d m i n sp\n"
      file << "FILLER fil ler sp\n"
    end

  end

  let :handler do
    TranscriptionsHandler.new
  end

  it "should create phone level transcriptions from master label file splitting into phonemes for filers" do

    handler.fetch_transcriptions_from_wrds({
      wrds_folder: "./Testing/data/corpus/",
      save_labs_in: "./Testing/data/MFCCs/",
    })

    "./Testing/data/MFCCs/f1ajrlp1.lab".should exist

    handler.generate_master_label_files(:filler, {
      phones_mlf: "./Testing/MLFs/phones.mlf",
      words_mlf: "./Testing/MLFs/mlf",
      files_list: "./Testing/training_list",
      phones_dict: "./Testing/phones_dict",
      fill_using: :words
    })

    "./Testing/MLFs/mlf".should exist
    "./Testing/MLFs/mlf".should include_content("FILLER")
    "./Testing/MLFs/mlf".should include_line("\"./Testing/data/MFCCs/f1ajrlp6.lab\"")

    "./Testing/MLFs/phones.mlf".should exist
    "./Testing/MLFs/phones.mlf".should include_content("fil\n")
    "./Testing/MLFs/phones.mlf".should include_content("ler\n")
  end

  it "should create word level transcriptions in master label file for fillers using intervals" do

    handler.fetch_transcriptions_from_wrds({
      wrds_folder: "./Testing/data/corpus/",
      save_labs_in: "./Testing/data/MFCCs/",
    })

    "./Testing/data/MFCCs/f1ajrlp1.lab".should exist

    handler.generate_master_label_files(:filler, {
      phones_mlf: "./Testing/MLFs/phones.mlf",
      words_mlf: "./Testing/MLFs/mlf",
      files_list: "./Testing/training_list",
      phones_dict: "./Testing/phones_dict",
      fill_using: :intervals,
      interval_in_seconds: 1
    })

    "./Testing/MLFs/mlf".should exist
    "./Testing/MLFs/mlf".should include_line("000000000  010000000  FILLER")
    "./Testing/MLFs/mlf".should include_line("450000000  453300020  FILLER")
    "./Testing/MLFs/mlf".should include_line("\"./Testing/data/MFCCs/f1ajrlp6.lab\"")
  end

  it "should create phone level transcriptions from master label file splitting into phonemes for keywords" do

    handler.fetch_transcriptions_from_wrds({
      wrds_folder: "./Testing/data/corpus/",
      save_labs_in: "./Testing/data/MFCCs/",
    })

    handler.generate_master_label_files(:keywords, {
      phones_mlf: "./Testing/MLFs/phones.mlf",
      words_mlf: "./Testing/MLFs/mlf",
      files_list: "./Testing/training_list",
      phones_dict: "./Testing/phones_dict",
      keywords: %w(key1 justice NineteeN KEY4),
    })
    "./Testing/MLFs/mlf".should_not include_line("11.030000  11.500000  {EXPECTED")
    "./Testing/MLFs/mlf".should include_line("1245900000  12045900000  NINETEEN")

    "./Testing/data/MFCCs/f1ajrlp2.lab".should exist
    "./Testing/data/MFCCs/f1ajrlp2.lab".should include_content("JUSTICE")
    "./Testing/data/MFCCs/f1ajrlp2.lab".should include_content("NINETEEN")

    "./Testing/MLFs/phones.mlf".should exist
    "./Testing/MLFs/phones.mlf".should include_line("26800000 27657143 j")
    "./Testing/MLFs/phones.mlf".should include_line("27657143 28514285 ah")
  end

  it "should prepare transcriptions filling every 1 sec" do

    handler.fetch_transcriptions_from_wrds({
      wrds_folder: "./Testing/data/corpus/",
      save_labs_in: "./Testing/data/MFCCs/",
    })

    handler.generate_master_label_files(:merged, {
      phones_mlf: "./Testing/MLFs/phones.mlf",
      words_mlf: "./Testing/MLFs/mlf",
      files_list: "./Testing/training_list",
      phones_dict: "./Testing/phones_dict",
      keywords: %w(key1 justice ADMINISTRATION KEY4),
      fill_using: :intervals,
      interval_in_seconds: 1, 
    })

    "./Testing/MLFs/mlf".should include_line("173500000  181600000  ADMINISTRATION")
    "./Testing/MLFs/mlf".should include_line("181600000  191600000  FILLER")
    "./Testing/MLFs/mlf".should include_line("191600000  201600000  FILLER")
       
  end

end




