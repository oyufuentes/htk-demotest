require 'spec_helper'

describe SplitTesting do

  let(:files){
    [:model, :phones_dict, :phones_list, :words_sorted]
  }

  let(:options){
    options = {
      mfccs_folder: "./Testing/Data/MfCCs",
      testing_list:  "./Testing/tests.list",
      reference_mlf: "./Testing/reference_mlf",
      macros: "./Testing/macros", 
      results: "./Testing/test/results",
      cleaned_results: "./Testing/test/cleaned_results",
      filler_probability: 0.5,
      transition_penality: 0,
      grammar: "./Testing/grammar",
      grammar_wdnet: "./Testing/grammar_wdnet",
      likehoods_scaling: 0.5,
      words_sorted: "./Testing/words_sorted",
      fill_using: :intervals,
      increment_mixtures_by: "+2"
    }

    files.each do |file|
      ["filler_", "keywords_", ''].each do |prefix|
        filename = "#{prefix}#{file}".to_sym
        options[filename] = "./Testing/#{filename}"
      end
    end
    options
  }

  it "can prepare grammar" do
    "./Testing/tests.list".write "list"
    
    testing = SplitTesting.new(options)

    testing.prepare_grammar(["key1","key2"])
    "./Testing/grammar".should include_content("$keyword = KEY1 | KEY2;")
    "./Testing/grammar_wdnet".should include_content("l=-0.69")
  end

 
end