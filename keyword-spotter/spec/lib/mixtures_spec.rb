require 'spec_helper'

describe Mixtures do
  it "calculates right mixtures" do
  	Mixtures.on("spec/data/models/mixtures\ example/hmmdefs", :filler).should eq 5
  	Mixtures.on("spec/data/models/mixtures\ example/hmmdefs", :keywords).should eq 3
  end
end