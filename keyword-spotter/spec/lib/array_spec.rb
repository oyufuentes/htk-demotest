require 'spec_helper'

describe "Array helper" do
  it "intercalate elements" do
    ([:a]*10).intercalate(:elem, 4).should eq([:a, :a, :a, :a, :elem, :a, :a, :a, :a, :elem, :a, :a])
    ([:a]*8).intercalate(:elem, 4).should eq([:a, :a, :a, :a, :elem, :a, :a, :a, :a])
  end

  it "calculates sums and averages" do
    [1,2,3,4].sum.should eq(10)
    [1,2,3,4].average.should eq(2.5)
    [1,2,3,4].weighted_average([2,1,1,1]).should eq(2.2)
  end
end



