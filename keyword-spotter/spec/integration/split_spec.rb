require 'spec_helper'

describe "split keyword-filler integration test" do
  include_context "integration"

  it "should train filler models", slow: true do
    generate_cross_validation_lists

    expect {
      train_filler_model(8)
    }.to_not raise_error

    "./Testing/model/filler/hmm_0".should exist

    #3 Re-estimations:
    "./Testing/model/filler/hmm_1".should exist
    "./Testing/model/filler/hmm_2".should exist
    "./Testing/model/filler/hmm_3".should exist

    # Duplicating mixtures:
    "./Testing/model/filler/hmm_4/hmmdefs".should include_content("<NUMMIXES> 3")
    
    #3 Re-estimations: 
    "./Testing/model/filler/hmm_5".should exist
    "./Testing/model/filler/hmm_6".should exist
    "./Testing/model/filler/hmm_7".should exist

    "./Testing/model/filler/hmm_8/hmmdefs".should include_content("<NUMMIXES> 5")
    
    #2 Re-estimations
    "./Testing/model/filler/hmm_9".should exist
    "./Testing/model/filler/hmm_10".should exist
    "./Testing/model/filler/hmm_11".should_not exist

  end

  it "should train keyword models", slow: true do
    generate_cross_validation_lists

    expect {
      train_keywords_model
    }.to_not raise_error

    "./Testing/model/keywords/hmm_0".should exist

    #Re-estimations:
    "./Testing/model/keywords/hmm_1".should exist
    "./Testing/model/keywords/hmm_2".should exist
    "./Testing/model/keywords/hmm_3/hmmdefs".should_not include_content('~h "sp"')

    # Fixing silence models:
    "./Testing/model/keywords/hmm_4/hmmdefs".should include_content('~h "sp"')
    "./Testing/model/keywords/hmm_5".should exist
    "./Testing/model/keywords/hmm_6".should exist

    # Duplicating mixtures:
    "./Testing/model/keywords/hmm_9/hmmdefs".should_not include_content("<NUMMIXES> 5")
    "./Testing/model/keywords/hmm_10/hmmdefs".should include_content("<NUMMIXES> 5")

    "./Testing/data2/training.list".should exist
    "./Testing/data2/testing.list".should exist

  end

  it "can continue training", slow: true do
    generate_cross_validation_lists
    training = train_keywords_model(4)
    # FileUtils.rm_rf "./Testing/model/keywords/hmm_7"
    FileUtils.rm_rf "./Testing/model/keywords/hmm_6"
    FileUtils.rm_rf "./Testing/model/keywords/hmm_5"
    FileUtils.rm_rf "./Testing/model/keywords/hmm_4"
    FileUtils.rm_rf "./Testing/model/keywords/hmm_3"
    FileUtils.rm_rf "./Testing/model/keywords/hmm_2"
    FileUtils.rm_rf "./Testing/model/keywords/hmm_1"
    FileUtils.rm_rf "./Testing/model/keywords/hmm_0"
    puts "----------------------------------------"
    expect {
      train_keywords_model
    }.to_not raise_error

    "./Testing/model/keywords/hmm_9".should exist
    "./Testing/model/keywords/hmm_10/hmmdefs".should include_content("<NUMMIXES> 5")

  end

  it "should test trained models", slow: true do
    generate_cross_validation_lists
    create_grammar
    train_keywords_model
    train_filler_model(3)

    expect {
      test_models
    }.to_not raise_error
  end

  def create_grammar
    grammar = Grammar.new(:merged, {
      grammar: "./Testing/grammar",
      grammar_wdnet: "./Testing/grammar.wdnet",
      keywords: keywords,
    })
  end

  def train_keywords_model(reestimations = 8)
    Training.train(:keywords, {
      on: "./Testing/model/keywords/",
      keywords: keywords,
      states: 5,
      mfccs_folder: "./Testing/data/mfccs",
      testing_list:  "./Testing/data/tests.list",
      training_list: "./Testing/data/training.list",
      copy_data_on: "./Testing/data2/",
      training_phones_mlf: "./Testing/data/keywords/mlfs/train-phones.mlf",
      training_words_mlf: "./Testing/data/keywords/mlfs/train-words.mlf",
      words_sorted: "./Testing/dictionary/keywords/words-sorted.list",
      phones_dict: "./Testing/dictionary/keywords/phones.dict",
      phones_list: "./Testing/dictionary/keywords/phones.list",
      split_training_every: 2,  
      herest_scripts_path: "./Testing/scripts/herest/",
      reestimations: reestimations,
      increment_mixtures_every: 3,
      increment_mixtures_by: "+2",
      dictionary: "./sources/dictionary/dict",
      word_as_unit: false,
      filler_every: 0.5,
      fill_using: :intervals,
    })
  end

  def train_filler_model(reestimations = 5)
    Training.train(:filler, {
      on: "./Testing/model/filler/",
      states: 5,
      mfccs_folder: "./Testing/data/mfccs",
      testing_list:  "./Testing/data/tests.list",
      training_list: "./Testing/data/training.list",
      copy_data_on: "./Testing/data2/",
      training_phones_mlf: "./Testing/data/filler/mlfs/train-phones.mlf",
      training_words_mlf: "./Testing/data/filler/mlfs/train-words.mlf",
      wrds_folder: "./Testing/data/wavs/",
      words_sorted: "./Testing/dictionary/filler/words-sorted.list",
      phones_dict: "./Testing/dictionary/filler/phones.dict",
      phones_list: "./Testing/dictionary/filler/phones.list",
      split_training_every: 2,
      herest_scripts_path: "./Testing/scripts/herest/",
      reestimations: reestimations,
      increment_mixtures_every: 3,
      increment_mixtures_by: "+2",
      word_as_unit: false,
      fill_using: :intervals,
      filler_every: 0.5,
    })
  end

  def test_models
    testing = SplitTesting.new({
      keywords_model: "./Testing/model/keywords/hmm_7/hmmdefs",
      filler_model: "./Testing/model/filler/hmm_3/hmmdefs",
      model: "./Testing/model/merged/hmmdefs",

      keywords_words_sorted: "./Testing/dictionary/keywords/words-sorted.list",
      filler_words_sorted: "./Testing/dictionary/filler/words-sorted.list",
      words_sorted: "./Testing/dictionary/merged/words_sorted.list",

      keywords_phones_list: "./Testing/dictionary/keywords/phones.list",
      filler_phones_list: "./Testing/dictionary/filler/phones.list",
      phones_list: "./Testing/dictionary/merged/phones.list",

      keywords_phones_dict: "./Testing/dictionary/keywords/phones.dict",
      filler_phones_dict: "./Testing/dictionary/filler/phones.dict",
      phones_dict: "./Testing/dictionary/merged/phones.dict",

      grammar: "./Testing/grammar",
      grammar_wdnet: "./Testing/grammar.wdnet",
      filler_probability: 0.5,
      transition_penality: 0,

      testing_list: "./Testing/data/tests.list",
      macros: "./Testing/model/keywords/hmm_7/macros",
      results: "./Testing/testing_results",
      cleaned_results: "./Testing/testing_results_cleaned",

      wrds_folder: "./Testing/data/wavs/",
      mfccs_folder: "./Testing/data/mfccs",
      reference_mlf: "./Testing/data/reference.mlf",
      likehoods_scaling: 0.5,
    })

    testing.start
  end

  def keywords
    ["Massachusetts", "Boston", "security"]
  end

end





