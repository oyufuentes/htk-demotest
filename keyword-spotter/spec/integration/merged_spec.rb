require 'spec_helper'

describe "merged models integration test" do
  include_context "integration"

  it "should train merged models with different mixtures distributions" do
    generate_cross_validation_lists
    expect{
      create_grammar
      train_merged_models(8, {
        increment_filler_mixtures_by: "*2",
        filler_states: 7,
      })
    }.not_to raise_error
    Mixtures.on("./Testing/model/merged/hmm_7/hmmdefs", :filler).should eq(2)
    Mixtures.on("./Testing/model/merged/hmm_7/hmmdefs", :keywords).should eq(3)
  end

  it "should test merged models", slow: true do
    generate_cross_validation_lists
    expect{
      create_grammar
      train_merged_models
      test_models
    }.not_to raise_error

    "./Testing/model/merged/hmm_7/hmmdefs".should include_content('~h "fill"')
    "./Testing/model/merged/hmm_7/hmmdefs".should include_content('~h "boston"')
  end

  def create_grammar
    grammar = Grammar.new(:merged, {
      grammar: "./Testing/grammar",
      grammar_wdnet: "./Testing/grammar.wdnet",
      keywords: keywords,
    })
  end

  def train_merged_models(reestimations = 8, options = {})
    options = options.reverse_merge({
      on: "./Testing/model/merged/",
      keywords: keywords,
      states: 5,
      filler_states: 7,
      filler_kind: :left_right,
      testing_list:  "./Testing/data/tests.list",
      training_list: "./Testing/data/training.list",
      mfccs_folder: "./Testing/data/mfccs/",
      copy_data_on: "./Testing/data2/",
      training_phones_mlf: "./Testing/data/merged/mlfs/train-phones.mlf",
      training_words_mlf: "./Testing/data/merged/mlfs/train-words.mlf",
      words_sorted: "./Testing/dictionary/merged/words-sorted.list",
      phones_dict: "./Testing/dictionary/merged/phones.dict",
      phones_list: "./Testing/dictionary/merged/phones.list",
      split_training_every: 2,
      herest_scripts_path: "./Testing/scripts/herest/",
      reestimations: reestimations,
      increment_mixtures_every: 3,
      increment_mixtures_by: "+2",
      dictionary: "./sources/dictionary/dict",
      word_as_unit: true,
      fill_using: :intervals,
      filler_every: 0.5,
    })
    Training.train(:merged, options)
  end

  def test_models
    testing = MergedTesting.new({
      model: "./Testing/model/merged/hmm_7/hmmdefs",
      macros: "./Testing/model/merged/hmm_7/macros",

      phones_list: "./Testing/dictionary/merged/phones.list",
      phones_dict: "./Testing/dictionary/merged/phones.dict",

      grammar: "./Testing/grammar",
      grammar_wdnet: "./Testing/grammar.wdnet",

      transition_penality: 0,
      keywords: keywords,

      testing_list: "./Testing/data/tests.list",
      results: "./Testing/testing_results",
      cleaned_results: "./Testing/testing_results_cleaned",
      words_sorted: "./Testing/dictionary/merged/words-sorted.list",

      wrds_folder: "./Testing/data/wavs/",
      mfccs_folder: "./Testing/data/mfccs",
      reference_mlf: "./Testing/data/reference.mlf",
      likehoods_scaling: 5,
    })

    testing.start
  end

  def keywords
    ["Massachusetts", "Boston", "security"]
  end

end





