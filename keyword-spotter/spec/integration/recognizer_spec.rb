require 'spec_helper'

describe "general recognizer test", integration: true, slow: true do
  include_context "integration"

  it "should train and test a recognizer (all words as keywords)" do
    expect {
      generate_cross_validation_lists
      create_grammar
      train_recognizer_models
      test_models
    }.to_not raise_error
  end

  def create_grammar
    grammar = Grammar.new(:recognizer, {
      grammar: "./Testing/grammar",
      grammar_wdnet: "./Testing/grammar.wdnet",
      all_words: all_words,
    })
  end

  def train_recognizer_models(reestimations = 7)
    Training.train(:recognizer, {
      on: "./Testing/model/recognizer/",
      states: 5,
      mfccs_folder: "./Testing/data/mfccs",
      testing_list:  "./Testing/data/tests.list",
      training_list: "./Testing/data/training.list",
      copy_data_on: "./Testing/data2/",
      tests_list: "./Testing/data/tests.list",
      training_phones_mlf: "./Testing/data/recognizer/mlfs/train-phones.mlf",
      training_words_mlf: "./Testing/data/recognizer/mlfs/train-words.mlf",
      words_sorted: "./Testing/dictionary/recognizer/words-sorted.list",
      phones_dict: "./Testing/dictionary/recognizer/phones.dict",
      phones_list: "./Testing/dictionary/recognizer/phones.list",
      split_training_every: 2,
      herest_scripts_path: "./Testing/scripts/herest/",
      reestimations: reestimations,
      increment_mixtures_every: 5,
      increment_mixtures_by: "+2",
      dictionary: "./sources/dictionary/dict",
      all_words: all_words,
      word_as_unit: false
    })
  end

  def test_models
    testing = RecognizerTesting.new({
      model: "./Testing/model/recognizer/hmm_7/hmmdefs",
      macros: "./Testing/model/recognizer/hmm_7/macros",

      phones_list: "./Testing/dictionary/recognizer/phones.list",
      phones_dict: "./Testing/dictionary/recognizer/phones.dict",

      grammar: "./Testing/grammar",
      grammar_wdnet: "./Testing/grammar.wdnet",

      transition_penality: 0,
      all_words: all_words,

      testing_list: "./Testing/data/tests.list",
      results: "./Testing/testing_results",
      cleaned_results: "./Testing/testing_results_cleaned",

      wrds_folder: "./Testing/data/wavs/",
      mfccs_folder: "./Testing/data/mfccs",
      reference_mlf: "./Testing/data/reference.mlf",
      likehoods_scaling: 5,
      
      words_sorted: "./Testing/dictionary/recognizer/words-sorted.list",
    })

    testing.start
  end

  def all_words
    Keywords.all_words({
      wrds_source: "./Testing/data/wavs/",
      dictionary: "./sources/dictionary/dict",
    })
  end

end





